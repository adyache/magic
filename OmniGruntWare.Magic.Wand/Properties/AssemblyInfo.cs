﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("OmniGruntWare.Magic.Wand")]
[assembly: AssemblyDescription("Public Interfaces for the Magic VS extension")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("OmniGruntWare")]
[assembly: AssemblyProduct("Magic.Wand")]
[assembly: AssemblyCopyright("Copyright © 2014 OmniGruntWare.  Magic Wand Icon by Umar123 (http://www.designkindle.com/2011/10/07/build-icons)")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f1fe4619-b004-4ed7-a129-ebdee97abc6d")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.14.0")]
[assembly: AssemblyFileVersion("1.0.14.0")]
