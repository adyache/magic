﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

namespace OmniGruntWare.Magic.Wand
{
    /// <summary>
    /// The interface that must be implemented by every Magic spell.
    /// <para>Magic scans the Spells assembly after every solution build for ISpell implementations.
    /// </para>Any implementations found are exposed on the Tools - Magic menu in the Visual Studio IDE.
    /// </summary>
    public interface ISpell
    {
        /// <summary>
        /// The name of the spell.  May include an ampersand (&amp;) to indicate a Windows hotkey.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The entry point for the spell, called when the user clicks the spell's menu item on the Tools - Magic menu.
        /// </summary>
        /// <param name="env">An instance that exposes Magic features to the spell during execution</param>
        void Execute(IDevelopmentEnvironment env);
    }
}
