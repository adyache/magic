﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;

namespace OmniGruntWare.Magic.Wand
{
    /// <summary>
    /// Provides a managed builder for creating user prompts from within a spell
    /// </summary>
    public interface IPromptSyntax
    {
        /// <summary>
        /// Sets the minimum width for the form to override the normal auto-sizing based on content. 
        /// </summary>
        /// <param name="width">the smallest width the form should have.</param>
        /// <returns>The current builder, so that additional controls can be added</returns>
        ICompletedPromptSyntax MinWidth(int width);

        /// <summary>
        /// Adds an instructions paragraph to the input form using a full-width, auto-height label.
        /// </summary>
        /// <param name="text">Text to set for the label</param>
        /// <returns>The current builder, so that additional controls can be added</returns>
        ICompletedPromptSyntax Instructions(string text);

        /// <summary>
        /// Adds an Up/Down control to the input form for integer entry
        /// </summary>
        /// <param name="name">Name for the control, also used as the entry's key after the form closes</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptIntSyntax ForInteger(string name);

        /// <summary>
        /// Adds a Textbox control to the input form for string entry
        /// </summary>
        /// <param name="name">Name for the control, also used as the entry's key after the form closes</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptStringSyntax ForString(string name);

        /// <summary>
        /// Adds a Dropdown control to the input form for a "select one" entry
        /// </summary>
        /// <param name="name">Name for the control, also used as the entry's key after the form closes</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptSelectSyntax ForSelection(string name);

        /// <summary>
        /// Adds a Checkbox control to the input form for boolean entry
        /// </summary>
        /// <param name="name">Name for the control, also used as the entry's key after the form closes</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptBoolSyntax ForBoolean(string name);

        /// <summary>
        /// Adds a Calendar control to the input form for date entry
        /// </summary>
        /// <param name="name">Name for the control, also used as the entry's key after the form closes</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptDateSyntax ForDate(string name);
    }

    /// <summary>
    /// Defines additional settings that can be specified for a checkbox
    /// </summary>
    public interface IPromptBoolSyntax
    {
        /// <summary>
        /// Sets the label to display next to the checkbox
        /// </summary>
        /// <param name="label">Label text to display.  May include an ampersand (&amp;) to indicate a hotkey</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptBoolLabelSyntax Label(string label);
    }

    /// <summary>
    /// Defines the default value used for the checkbox
    /// </summary>
    public interface IPromptBoolLabelSyntax
    {
        /// <summary>
        /// Sets the fixed default value to use for the checkbox
        /// </summary>
        /// <param name="value">Default value to use</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IDisabledBySyntax Default(bool value);

        /// <summary>
        /// Instructs the form to use the last successful entry as the default value for the checkbox, or fall back to the specified value.
        /// <para>The last successful entries are persisted in a key/value dictionary with the control's name as the key.
        /// </para>Therefore, different prompts can share the same default value by using the same name for a control.
        /// </summary>
        /// <param name="value">The default value to use if a previous successful value is not available</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IDisabledBySyntax PreviousDefaultOr(bool value);
    }

    /// <summary>
    /// Allows to complete the input form or add more controls
    /// </summary>
    public interface ICompletedPromptSyntax : IPromptSyntax, IPreparedPromptSyntax
    {
        /// <summary>
        /// Sets a validator callback for complex validations, such as multiple-field validations.
        /// </summary>
        /// <param name="validator">Callback method to call to validate the entries.
        /// <para>The validator is called only if all built-in validations (required, min/max, etc.) are satisfied.
        /// </para>Any controls that are disabled by others are NOT validated, and are excluded from the results dictionary supplied to the validator.
        /// <para>Any enabled controls are guaranteed to have corresponding entries in the results dictionary.
        /// </para>For non-required controls, the values in the dictionary may be null.
        /// <para>Values that are not null will be strongly typed according to the type of control.
        /// </para>The validator receives a dictionary of the user entries, and is expected to return zero or more errors.
        /// <para>If the validator returns any errors, they are displayed in a message box and the form remains open.
        /// </para>The form cannot be accepted if there are any errors, but it can be cancelled.</param>
        /// <returns>The current builder, so that the prompt can be executed</returns>
        IPreparedPromptSyntax Validate(Func<IReadOnlyDictionary<string, object>, IEnumerable<string>> validator = null);
    }

    /// <summary>
    /// Allows to execute the completed input form
    /// </summary>
    public interface IPreparedPromptSyntax
    {
        /// <summary>
        /// Finishes building the input form, displays it, and gets the values entered.
        /// </summary>
        /// <returns>If the form is successfully accepted and validated, a dictionary of user entries keyed on control name.
        /// <para>Null indicates that the form was cancelled.
        /// </para>Any controls that are disabled by others are NOT validated, and are excluded from the results dictionary.
        /// <para>Any enabled controls are guaranteed to have corresponding entries in the results dictionary.
        /// </para>For non-required controls, the values in the dictionary may be null.
        /// <para>Values that are not null will be strongly typed according to the type of control.
        /// </para>The "last successful" entries are saved for reuse only if the form is successfully accepted, and only for enabled controls.</returns>
        IReadOnlyDictionary<string, object> Run();
    }

    /// <summary>
    /// Defines additional settings that can be specified for an up/down control
    /// </summary>
    public interface IPromptIntSyntax
    {
        /// <summary>
        /// Sets the label to display next to the up/down control
        /// </summary>
        /// <param name="label">Label text to display.  May include an ampersand (&amp;) to indicate a hotkey</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptIntLabelSyntax Label(string label);
    }

    /// <summary>
    /// Defines additional settings that can be specified for an integer up/down control
    /// </summary>
    public interface IPromptIntLabelSyntax
    {
        /// <summary>
        /// Sets the minimum allowed value for an up/down control
        /// </summary>
        /// <param name="value">the lowest allowed value</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptIntMinSyntax Minimum(int value);
    }

    /// <summary>
    /// Defines additional settings that can be specified for an integer up/down control
    /// </summary>
    public interface IPromptIntMinSyntax
    {
        /// <summary>
        /// Sets the maximum allowed value for an up/down control
        /// </summary>
        /// <param name="value">The highest allowed value</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptIntMaxSyntax Maximum(int value);
    }

    /// <summary>
    /// Defines the default value used for the up/down control
    /// </summary>
    public interface IPromptIntMaxSyntax
    {
        /// <summary>
        /// Sets the fixed default value to use for the up/down control
        /// </summary>
        /// <param name="value">Default value to use</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IRequiredSyntax Default(int value);

        /// <summary>
        /// Instructs the form to use the last successful entry as the default value for the up/down control, or fall back to the specified value.
        /// <para>The last successful entries are persisted in a key/value dictionary with the control's name as the key.
        /// </para>Therefore, different prompts can share the same default value by using the same name for a control.
        /// </summary>
        /// <param name="value">The default value to use if a previous successful value is not available</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IRequiredSyntax PreviousDefaultOr(int value);
    }

    /// <summary>
    /// Allows setting up a control as required.
    /// </summary>
    public interface IRequiredSyntax : IDisabledBySyntax
    {
        /// <summary>
        /// Require the control to have a value entered before the form can accept an OK button click.
        /// <para>Required validation is suppressed if a control is disabled by <see cref="IDisabledBySyntax.DisabledBy"/> or <see cref="IDisabledBySyntax.EnabledBy"/>.
        /// </para>
        /// </summary>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IDisabledBySyntax Required();
    }

    /// <summary>
    /// Allows a control to be conditionally enabled or disabled by a checkbox on the form.
    /// </summary>
    public interface IDisabledBySyntax : ICompletedPromptSyntax
    {
        /// <summary>
        /// Configure the control to be disabled when the checkbox with the specified name is checked.
        /// </summary>
        /// <param name="name">The name of the checkbox that will disable this control</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        ICompletedPromptSyntax DisabledBy(string name);

        /// <summary>
        /// Configure the control to be enabled only when the checkbox with the specified name is checked.
        /// </summary>
        /// <param name="name">The name of the checkbox that will enable this control</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        ICompletedPromptSyntax EnabledBy(string name);
    }

    /// <summary>
    /// Defines additional settings that can be specified for a textbox
    /// </summary>
    public interface IPromptStringSyntax
    {
        /// <summary>
        /// Sets the label to display next to the textbox
        /// </summary>
        /// <param name="label">Label text to display.  May include an ampersand (&amp;) to indicate a hotkey</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptStringLabelSyntax Label(string label);
    }

    /// <summary>
    /// Configures whether the textbox is multiline
    /// </summary>
    public interface IPromptStringLabelSyntax : IPromptStringMultiLineSyntax
    {
        /// <summary>
        /// Makes the textbox multi-line.
        /// </summary>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptStringMultiLineSyntax Multiline();
    }

    /// <summary>
    /// Defines the default value used for the textbox
    /// </summary>
    public interface IPromptStringMultiLineSyntax
    {
        /// <summary>
        /// Sets the fixed default value to use for the textbox
        /// </summary>
        /// <param name="value">Default value to use</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IRequiredSyntax Default(string value);

        /// <summary>
        /// Instructs the form to use the last successful entry as the default value for the textbox, or fall back to the specified value.
        /// <para>The last successful entries are persisted in a key/value dictionary with the control's name as the key.
        /// </para>Therefore, different prompts can share the same default value by using the same name for a control.
        /// </summary>
        /// <param name="value">The default value to use if a previous successful value is not available</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IRequiredSyntax PreviousDefaultOr(string value);
    }

    /// <summary>
    /// Defines additional settings that can be specified for a date control
    /// </summary>
    public interface IPromptDateSyntax
    {
        /// <summary>
        /// Sets the label to display next to the date control
        /// </summary>
        /// <param name="label">Label text to display.  May include an ampersand (&amp;) to indicate a hotkey</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptDateLabelSyntax Label(string label);
    }

    /// <summary>
    /// Defines additional settings that can be specified for a date control
    /// </summary>
    public interface IPromptDateLabelSyntax
    {
        /// <summary>
        /// Sets the minimum allowed value for a date control
        /// </summary>
        /// <param name="value">the lowest allowed value</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptDateMinSyntax Minimum(DateTime value);
    }

    /// <summary>
    /// Defines additional settings that can be specified for a date control
    /// </summary>
    public interface IPromptDateMinSyntax
    {
        /// <summary>
        /// Sets the minimum allowed value for a date control
        /// </summary>
        /// <param name="value">the lowest allowed value</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptDateMaxSyntax Maximum(DateTime value);
    }

    /// <summary>
    /// Defines the default value used for the date control
    /// </summary>
    public interface IPromptDateMaxSyntax
    {
        /// <summary>
        /// Sets the fixed default value to use for the date control
        /// </summary>
        /// <param name="value">Default value to use</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IRequiredSyntax Default(DateTime value);

        /// <summary>
        /// Instructs the form to use the last successful entry as the default value for the date control, or fall back to the specified value.
        /// <para>The last successful entries are persisted in a key/value dictionary with the control's name as the key.
        /// </para>Therefore, different prompts can share the same default value by using the same name for a control.
        /// </summary>
        /// <param name="value">The default value to use if a previous successful value is not available</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IRequiredSyntax PreviousDefaultOr(DateTime value);
    }

    /// <summary>
    /// Defines additional settings that can be specified for a dropdown
    /// </summary>
    public interface IPromptSelectSyntax
    {
        /// <summary>
        /// Sets the label to display next to the dropdown
        /// </summary>
        /// <param name="label">Label text to display.  May include an ampersand (&amp;) to indicate a hotkey</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptSelectLabelSyntax Label(string label);
    }

    /// <summary>
    /// Defines additional settings that can be specified for a dropdown
    /// </summary>
    public interface IPromptSelectLabelSyntax
    {
        /// <summary>
        /// Sets the options that should be displayed in the dropdown
        /// </summary>
        /// <param name="options">The options to display in the dropdown</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptSelectFromSyntax From(params string[] options);
    }

    /// <summary>
    /// Defines the default value used for the dropdown
    /// </summary>
    public interface IPromptSelectFromSyntax
    {
        /// <summary>
        /// Sets the fixed default value to use for the dropdown control
        /// </summary>
        /// <param name="value">Default value to use</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptSelectEnterableSyntax Default(string value);

        /// <summary>
        /// Instructs the form to use the last successful entry as the default value for the dropdown control, or fall back to the specified value.
        /// <para>The last successful entries are persisted in a key/value dictionary with the control's name as the key.
        /// </para>Therefore, different prompts can share the same default value by using the same name for a control.
        /// </summary>
        /// <param name="value">The default value to use if a previous successful value is not available</param>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IPromptSelectEnterableSyntax PreviousDefaultOr(string value);
    }

    /// <summary>
    /// Defines additional settings that can be specified for a dropdown control
    /// </summary>
    public interface IPromptSelectEnterableSyntax : IRequiredSyntax
    {
        /// <summary>
        /// Enables the dropdown to receive typed entries not on the list of options
        /// </summary>
        /// <returns>The current builder, so that additional parameters can be specified</returns>
        IRequiredSyntax AllowFreeEntry();
    }
}
