﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System.Collections.Generic;

namespace OmniGruntWare.Magic.Wand
{
    /// <summary>
    /// Provides access to solution projects and solution-level objects.
    /// <para>Note: some solution objects are not exposed by the Visual Studio SDK, and will not be available.
    /// </para>For example, the SDK does not seem to have a way to expose solution folders.
    /// </summary>
    public interface ISolutionSyntax : IFolderSyntax
    {
    }

    /// <summary>
    /// Provides access to a project's items.
    /// </summary>
    public interface IProjectSyntax : IFolderSyntax
    {
    }

    /// <summary>
    /// Exposes a folder in the solution tree for inspection or modification.
    /// </summary>
    public interface IFolderSyntax : IFileContainerSyntax
    {
        /// <summary>
        /// Gets a project from the solution or parent folder by name.
        /// </summary>
        /// <param name="name">The name of the project needed</param>
        /// <returns>An instance that provides access to the project's items</returns>
        IProjectSyntax Project(string name);

        /// <summary>
        /// Gets a folder from a parent folder or project by name.
        /// </summary>
        /// <param name="name">The name of the folder needed</param>
        /// <returns>An instance that provides access to the folder's items</returns>
        IFolderSyntax Folder(string name);

        /// <summary>
        /// Gets the names of child folders contained in a parent folder or project.
        /// </summary>
        /// <returns>The names of child folders</returns>
        IEnumerable<string> GetFolders();
    }

    /// <summary>
    /// Exposes child items of a folder or file.
    /// </summary>
    public interface IFileContainerSyntax
    {
        /// <summary>
        /// Creates a new file with the specified name.
        /// </summary>
        /// <param name="name">The name of the new file to create</param>
        /// <returns>A file builder that allows setting the file's contents</returns>
        ICreateFileSyntax CreateFile(string name);

        /// <summary>
        /// Gets a file from a folder, or a child file from a parent file, by name.
        /// </summary>
        /// <param name="name">The name of the file needed</param>
        /// <returns>An instance that exposes the file for inspection or modification</returns>
        IFileSyntax File(string name);

        /// <summary>
        /// Gets the names of files in a folder or child files dependent on a parent file.
        /// </summary>
        /// <returns>The file names found under the current folder or file</returns>
        IEnumerable<string> GetFiles();
    }

    /// <summary>
    /// Exposes a file in the solution.
    /// </summary>
    public interface IFileSyntax : IExistingFileSyntax
    {
        /// <summary>
        /// Opens the file in the IDE.
        /// </summary>
        /// <returns>An instance that allows to make changes to the file</returns>
        IExistingFileSyntax Open();

        /// <summary>
        /// Gets whether the file exists
        /// </summary>
        bool Exists { get; }
    }

    /// <summary>
    /// Exposes a file for modification
    /// </summary>
    public interface IExistingFileSyntax : IFileContainerSyntax
    {
        /// <summary>
        /// Prepares to make a change to the file below the location of the first match on the provided regular expression.
        /// </summary>
        /// <param name="regex">The .Net regular expression to use when locating the edit point in the file</param>
        /// <returns>A builder that allows to specify what code changes to make</returns>
        ILocationSyntax After(string regex);

        /// <summary>
        /// Prepares to make a change to the file above the location of the first match on the provided regular expression.
        /// </summary>
        /// <param name="regex">The .Net regular expression to use when locating the edit point in the file</param>
        /// <returns>A builder that allows to specify what code changes to make</returns>
        ILocationSyntax Before(string regex);

        /// <summary>
        /// Prepares to make a change to the file at a location between the start and end points determined by two regular expressions.
        /// <para>Typically used with <see cref="IBetweenAndSyntax.Alphabetically"/> to insert a line or method in alphabetical order within the start and end points.
        /// </para>The initial call sets the starting point and must be followed by <see cref="IBetweenSyntax.And"/>.
        /// </summary>
        /// <param name="regex">The .Net regular expression to use when locating the starting point for the change</param>
        /// <returns>A builder that continues to define the end point for the change</returns>
        IBetweenSyntax Between(string regex);

        /// <summary>
        /// Prepares to make a change to the file inside the specified code region.
        /// <para>Typically used with <see cref="IBetweenAndSyntax.Alphabetically"/> to insert a line or method in alphabetical order within the region.
        /// </para>Otherwise, assumes the change must be made at the bottom of the region.
        /// </summary>
        /// <param name="name">The name of the code region to make a change in</param>
        /// <returns>A builder that allows to specify what code changes to make</returns>
        IBetweenAndSyntax InRegion(string name);


        /// <summary>
        /// Prepares to make a change to the file inside the specified method.
        /// <para>May be used with <see cref="IBetweenAndSyntax.Alphabetically"/> to insert a line or method in alphabetical order within the method.
        /// </para>Otherwise, assumes the change must be made at the bottom of the method.
        /// </summary>
        /// <param name="name">The name of the method to make a change in.
        /// <para>If the method is overloaded, the current release will use the first occurrence found regardless of arguments.
        /// </para>To modify a specific overload, use the <see cref="Between"/> ... <see cref="IBetweenSyntax.And"/> syntax.</param>
        /// <returns>A builder that allows to specify what code changes to make</returns>
        IBetweenAndSyntax InMethod(string name);

        /// <summary>
        /// Provides access to the containing folder.
        /// <para>Returns null if a file is located directly in a project.</para>
        /// </summary>
        IFolderSyntax Folder { get; }

        /// <summary>
        /// Provides access to the main file from a child file.
        /// <para>If called on a file that is not a child file, returns the same file.</para>
        /// </summary>
        IExistingFileSyntax MainFile { get; }

        /// <summary>
        /// Gets the contents of the file.  If the file is open in the IDE, the contents includes any unsaved changes.
        /// </summary>
        string Contents { get; }

        /// <summary>
        /// Gets the name of the current file.
        /// </summary>
        string Name { get; }
    }

    /// <summary>
    /// Required continuation after a <see cref="IExistingFileSyntax.Between"/> method
    /// </summary>
    public interface IBetweenSyntax
    {
        /// <summary>
        /// Sets the end point of the range in a file where to make a change.
        /// </summary>
        /// <param name="regex">The .Net regular expression to use when locating the end point of the range in the file to make a change</param>
        /// <returns>A builder that allows to specify what change to make</returns>
        IBetweenAndSyntax And(string regex);
    }

    /// <summary>
    /// Allows to optionally specify that text should be inserted in alphabetical order.
    /// <para>The alphabetic order is treated differentlny depending on the text inserted.
    /// </para>See <see cref="ILocationSyntax"/> for the available ways to insert text into a file.
    /// </summary>
    public interface IBetweenAndSyntax : ILocationSyntax
    {
        /// <summary>
        /// Allows to optionally specify that text should be inserted in alphabetical order.
        /// <para>The alphabetic order is treated differentlny depending on the text inserted.
        /// </para>See <see cref="ILocationSyntax"/> for the available ways to insert text into a file.
        /// </summary>
        /// <returns>A builder that allows to specify what changes to make</returns>
        ILocationSyntax Alphabetically();
    }

    /// <summary>
    /// Contains methods for making textual changes to a file.
    /// </summary>
    public interface ILocationSyntax
    {
        /// <summary>
        /// Insert one or more members such as methods, properties, etc.
        /// <para>Each member may contain multiple lines.
        /// </para>To insert individual lines, use <see cref="InsertLines"/>.
        /// </summary>
        /// <param name="bodies">one or more code fragments (methods, properties, etc.) to be inserted.
        /// <para>Each member may contain multiple lines.
        /// </para>If <see cref="IBetweenAndSyntax.Alphabetically"/> was used, the sort is applied member-wise, and the inserted members are separated if necessary.</param>
        /// <returns>A builder that can be used to continue adding changes to the same file</returns>
        IExistingFileSyntax InsertMembers(params string[] bodies);

        /// <summary>
        /// Insert one or more lines of text to the file.
        /// <para>Each line should not have embedded line breaks.
        /// </para>To insert multi-line chunks such as methods, use <see cref="InsertMembers"/>.
        /// </summary>
        /// <param name="lines">one or more code lines to be inserted.
        /// <para>Lines should not contain embedded line breaks.
        /// </para>If <see cref="IBetweenAndSyntax.Alphabetically"/> was used, the lines are sorted (trimmed) and possibly separated.</param>
        /// <returns>A builder that can be used to continue adding changes to the same file</returns>
        IExistingFileSyntax InsertLines(params string[] lines);
    }

    /// <summary>
    /// Allows to specify the contents of a newly created file
    /// </summary>
    public interface ICreateFileSyntax
    {
        /// <summary>
        /// Sets the contents of a newly created file.
        /// </summary>
        /// <param name="body">File contents to use for the new file</param>
        /// <returns>A builder to optionally perform edits to the file after it is created.
        /// <para>Another use is to create child files after the main file has been created.</para></returns>
        IFileSyntax WithBody(string body);
    }
}
