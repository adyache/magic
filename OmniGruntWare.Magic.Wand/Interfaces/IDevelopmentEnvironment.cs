﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

namespace OmniGruntWare.Magic.Wand
{
    /// <summary>
    /// The main access point of the Magic extension.
    /// <para>An executing spell receives an instance of this interface as an argument to the Execute method.
    /// </para>Provides access to all Magic features during spell execution.
    /// </summary>
    public interface IDevelopmentEnvironment
    {
        /// <summary>
        /// Provides access to solution projects and solution-level objects.
        /// <para>Some object types are not exposed by the SDK, and so are not available.
        /// </para>For example, the SDK does not seem to expose solution folders.
        /// </summary>
        ISolutionSyntax Solution { get; }

        /// <summary>
        /// Gets a project from the solution by name.
        /// </summary>
        /// <param name="name">The name of the project needed</param>
        /// <returns>An instance that provides access to the project's items</returns>
        IProjectSyntax Project(string name);

        /// <summary>
        /// Prompts for input from the user.
        /// <para>This method creates a form builder and sets the form's title to the specified text.
        /// </para>Call builder methods to add input controls to the form.
        /// </summary>
        /// <param name="title">Title to use for the input form</param>
        /// <returns>A form builder for adding input controls to the form</returns>
        IPromptSyntax Prompt(string title);

        /// <summary>
        /// Gets the file that is currently active in the IDE.
        /// </summary>
        IExistingFileSyntax ActiveFile { get; }
    }
}
