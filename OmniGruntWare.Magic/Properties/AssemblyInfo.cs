﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("OmniGruntWare.Magic")]
[assembly: AssemblyDescription("Visual Studio extension for development automation, a.k.a. code snippets on steroids.  Define your automation tasks in code using a fluent API.  Create files, add methods, prompt for user input.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("OmniGruntWare")]
[assembly: AssemblyProduct("Magic")]
[assembly: AssemblyCopyright("Copyright © 2014 OmniGruntWare.  Icons by Umar123 (http://www.designkindle.com/2011/10/07/build-icons),  HADezign (http://hadezign.com)")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]   
[assembly: ComVisible(false)]     
[assembly: CLSCompliant(false)]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: InternalsVisibleTo("OmniGruntWare.Magic.Tests, PublicKey=00240000048000009400000006020000002400005253413100040000010001004987483a44767427f158a3b5a3bdfdbf8a2b268415de56769e2fa0fb7b01201d878194d70a96aefa0d5445a9d8606bb83ac4a80484a6bf42a426dac12be2a9f624e3ffd4dcf6faa46fd9ac86aec51b0a8e9b67936f6a75c4001f25131a01072857752644395d4ebdd2701163bda890e2740c87c8c3e81e74bafe988546af7ebc")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion( "1.0.1.24" )]
[assembly: AssemblyFileVersion( "1.0.1.24" )]



