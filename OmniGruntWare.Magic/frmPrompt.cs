﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using OmniGruntWare.Magic.Fluent;

namespace OmniGruntWare.Magic
{
    internal partial class frmPrompt : Form
    {
        private readonly List<FormField> _fields;
        private readonly Func<IReadOnlyDictionary<string, object>, IEnumerable<string>> _validator;

        public frmPrompt(List<FormField> fields, string title, int minWidth, Func<IReadOnlyDictionary<string, object>, IEnumerable<string>> validator)
        {
            _fields = fields;
            _validator = validator;

            InitializeComponent();

            Text = title;
            if (minWidth > 0)
                MinimumSize = new Size(minWidth, 0);

            InitializeFields();
        }

        private void InitializeFields()
        {
            var controlsByField = new Dictionary<FormField, List<Control>>();
            var row = 0;
            foreach (var field in _fields)
            {
                var controls = field.CreateControls().ToList();
                controlsByField.Add(field, controls);
                foreach (var control in controls)
                {
                    EnsureEnoughTableRows(row);
                    SetControlInTableRow(control, row++);
                }

                IncreaseBottomMargin(controls.Last());
            }

            btnOK.TabIndex = row++;
            btnCancel.TabIndex = row++;

            WireUpEnabledByControls(controlsByField);
        }

        private void WireUpEnabledByControls(Dictionary<FormField, List<Control>> controlsByField)
        {
            foreach (var field in _fields)
            {
                foreach (var by in field.IsEnabledBy)
                {
                    var byField = _fields.FirstOrDefault(x => x.Name == by.Key);
                    if (byField == null)
                        throw new ArgumentException("Invalid Enabled/Disabled By field " + by.Key);
                    if (!(byField is BooleanField))
                        throw new ArgumentException("Disabled/Enabled by field must be a boolean");

                    var checkBox = ((CheckBox)table.Controls[byField.ControlName]);
                    var enable = by.Value;
                    foreach (var control in controlsByField[field])
                    {
                        var c = control;
                        c.Enabled = checkBox.Checked == enable;
                        checkBox.CheckedChanged += (o, e) => c.Enabled = ((CheckBox) o).Checked == enable;
                    }
                }
            }
        }

        private static void IncreaseBottomMargin(Control lastControl)
        {
            var margin = lastControl.Margin;
            margin.Bottom += 5;
            lastControl.Margin = margin;
        }

        private void SetControlInTableRow(Control control, int row)
        {
            control.AutoSize = true;
            control.TabIndex = row;
            table.RowStyles[row].SizeType = SizeType.AutoSize;
            table.Controls.Add(control, 0, row);
        }

        private void EnsureEnoughTableRows(int row)
        {
            if (row < table.RowCount) return;
            table.RowCount++;
            table.RowStyles.Add(new RowStyle());
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Size = new Size(
                Width - table.Width + table.Controls.Cast<Control>().Max(x => x.Width + x.Margin.Horizontal),
                Height - table.Height + table.Controls.Cast<Control>().Sum(x => x.Height + x.Margin.Vertical));

            table.SelectNextControl(table, true, true, true, true);
        }

        public IReadOnlyDictionary<string, object> Results
        {
            get { return GetActiveFields().ToDictionary(x => x.Name, x => x.GetValueFromControl(table.Controls[x.ControlName])); }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            var errors = GetValidationErrors();

            if (errors.Count == 0)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                //todo: highlight invalid controls
                MessageBox.Show(this, string.Join(Environment.NewLine, errors), "Invalid Value(s)");
            }
        }

        internal IList<string> GetValidationErrors()
        {
            var results = Results;
            var activeFields = GetActiveFields().ToList();
            var errors = activeFields.SelectMany(x => x.Validate(results[x.Name])).ToList();
            if (errors.Count == 0 && _validator != null)
                errors.AddRange(_validator(results));

            if (errors.Count == 0)
            {
                foreach (var field in activeFields)
                    field.SetLastValue(results[field.Name]);
                MagicSettings.Default.Save();
            }

            return errors;
        }

        private IEnumerable<FormField> GetActiveFields()
        {
            return _fields.Where(x => x.Name != null && table.Controls.ContainsKey(x.ControlName) && table.Controls[x.ControlName].Enabled);
        }
    }
}
