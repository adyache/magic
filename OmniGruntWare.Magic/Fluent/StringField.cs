﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System.Collections.Generic;
using System.Windows.Forms;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic.Fluent
{
    internal class StringField : FormField, IPromptStringSyntax, IPromptStringLabelSyntax
    {
        private bool _multiline;

        public StringField(FormBuilder builder) : base(builder)
        {
        }

        public IPromptStringLabelSyntax Label(string label)
        {
            Caption = label;
            return this;
        }

        public IRequiredSyntax Default(string value)
        {
            return SetDefault(value, false);
        }

        public IRequiredSyntax PreviousDefaultOr(string value)
        {
            return SetDefault(value, true);
        }

        public IPromptStringMultiLineSyntax Multiline()
        {
            _multiline = true;
            return this;
        }

        public override IEnumerable<Control> CreateControls()
        {
            yield return new Label
            {
                Name = ControlName + "_Label",
                Text = Caption,
            };
            var textbox = new TextBox
            {
                Name = ControlName,
                Text = (string)GetDefaultValue(),
                Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
            };
            
            if (_multiline)
            {
                textbox.Height *= 6;
                textbox.Multiline = true;
                //tried this out and decided not to do it.
                //textbox.AcceptsReturn = true;
                textbox.ScrollBars = ScrollBars.Both;
            }

            yield return textbox;
        }

        protected override object ValueFromString(string value)
        {
            return value;
        }

        protected override string ValueToString(object value)
        {
            return (string)value;
        }

        public override object GetValueFromControl(Control control)
        {
            return control.Text;
        }

        public override IEnumerable<string> Validate(object value)
        {
            if (IsRequired && string.IsNullOrWhiteSpace((string)value))
                yield return CleanCaption + " is required";
        }
    }
}