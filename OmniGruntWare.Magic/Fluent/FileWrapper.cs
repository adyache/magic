﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.IO;
using System.Linq;
using EnvDTE;
using EnvDTE80;
using OmniGruntWare.Magic.DteAdapters;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic.Fluent
{
    internal class FileWrapper : FileContainer, IFileSyntax, ICreateFileSyntax, IBetweenSyntax
    {
        private ProjectItem _item;

        public FileWrapper(DTE2 dte2, FileContainer parent, FolderWrapper folder, string name)
            : base(dte2, parent, folder, name)
        {
        }

        private string _startRegex;
        private string _endRegex;

        public ILocationSyntax After(string regex)
        {
            _startRegex = regex;
            _endRegex = null;
            return OpenForEditing();
        }

        public ILocationSyntax Before(string regex)
        {
            _startRegex = null;
            _endRegex = regex;
            return OpenForEditing();
        }

        public IBetweenSyntax Between(string regex)
        {
            _startRegex = regex;
            return this;
        }

        public IBetweenAndSyntax And(string regex)
        {
            _endRegex = regex;
            return OpenForEditing();
        }

        public IBetweenAndSyntax InRegion(string name)
        {
            _startRegex = @"^[\s\t]*#region[\s\t]+" + name + @"[\s\t]*$";
            _endRegex = "#endregion";
            return OpenForEditing();
        }

        public IBetweenAndSyntax InMethod(string name)
        {
            _startRegex = @"[\s\n\t]+" + name + @"[\s\n\t]*\([^)]*\)[\s\n\t]*{";
            _endRegex = EditPointWrapper.CloseOfBody;
            return OpenForEditing();
        }

        private EditPointWrapper OpenForEditing()
        {
            EnsureItem().Open();
            var doc = _item.Document;
            if (doc == null)
                throw new Exception("Editing this type of file is not supported");

            var document = doc.Object("TextDocument") as TextDocument;
            if (document == null)
                throw new Exception("Editing this type of file is not supported");

            return new EditPointWrapper(this, document, _startRegex, _endRegex);
        }

        public IExistingFileSyntax Open()
        {
            EnsureItem().Open().Activate();
            return this;
        }

        private ProjectItem EnsureItem()
        {
            if (_item == null)
            {
                var collection = _parent.Collection;
                if (collection != null)
                {
                    var item = collection[_name] as FileDteItem;

                    if (item != null)
                        _item = item.ProjectItem;
                }

                if (_item == null)
                    throw new FileNotFoundException("The requested project item does not exist", _name);
            }

            return _item;
        }

        public override DteItemCollection Collection
        {
            get
            {
                try
                {
                    return (_parent.Collection[_name] as FileDteItem).Collection;
                }
                catch
                {
                    return null;
                }
            }
        }

        public bool Exists
        {
            get
            {
                var collection = _parent.Collection;
                if (collection == null) return false;
                var item = collection[_name];
                return item is FileDteItem;
            }
        }

        protected internal override DteItemCollection Create()
        {
            for (var i = 0; i < 2; i++)
            {
                var collection = _parent.Create();
                var item = collection[_name];
                
                if (item is FileDteItem)
                    return item.Collection;

                WithBody("");
            }

            throw new Exception("Could not create file");
        }

        public IExistingFileSyntax MainFile
        {
            get
            {
                var parentFile = _parent as FileWrapper;
                return parentFile != null
                    ? parentFile.MainFile
                    : this;
            }
        }

        public string Contents
        {
            get
            {
                EnsureItem();

                if (_item.IsOpen)
                {
                    var doc = _item.Document;
                    if (doc != null)
                    {
                        var textDoc = doc.Object("TextDocument") as TextDocument;
                        if (textDoc != null)
                        {
                            var startPoint = textDoc.StartPoint.CreateEditPoint();
                            return startPoint.GetText(textDoc.EndPoint);
                        }
                    }
                }

                var fileName = System.IO.Path.Combine(_folder.Path, _item.Name);
                return System.IO.File.ReadAllText(fileName);
            }
        }

        public IFileSyntax WithBody(string body)
        {
            var path = Path;
            Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path));
            System.IO.File.WriteAllText(path, body);
            _parent.Collection.AddFromFile(path);
            return this;
        }

        public override IFileSyntax File(string name)
        {
            return new FileWrapper(_dte2, this, _folder, name);
        }
    }
}
