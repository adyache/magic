﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System.Windows.Forms.VisualStyles;
using EnvDTE;
using EnvDTE80;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic.Fluent
{
    internal class DevelopmentEnvironmentWrapper : IDevelopmentEnvironment
    {
        private readonly DTE2 _dte2;

        public DevelopmentEnvironmentWrapper(DTE2 dte2)
        {
            _dte2 = dte2;
        }

        public ISolutionSyntax Solution
        {
            get { return new SolutionWrapper(_dte2); }
        }

        public IProjectSyntax Project(string name)
        {
            return Solution.Project(name);
        }

        public IPromptSyntax Prompt(string title)
        {
            return new FormBuilder(_dte2, title);
        }

        public IExistingFileSyntax ActiveFile
        {
            get
            {
                var doc = _dte2.ActiveDocument;
                return doc != null
                    ? GetFileFromProjectItem(doc.ProjectItem)
                    : null;
            }
        }

        private IExistingFileSyntax GetFileFromProjectItem(ProjectItem item)
        {
            var name = item.Name;
            var parent = item.Collection.Parent;

            var parentContainer = 
                GetParentItem(parent as ProjectItem) ??
                GetParentFolder(parent as ProjectItem) ??
                GetParentProject(parent as Project);

            return parentContainer.File(name);
        }

        private IFolderSyntax GetFolderFromProjectItem(ProjectItem item)
        {
            var name = item.Name;
            var parent = item.Collection.Parent;

            var parentFolder = 
                GetParentFolder(parent as ProjectItem) ??
                GetParentProject(parent as Project);

            return parentFolder.Folder(name);
        }

        private IFileContainerSyntax GetParentItem(ProjectItem parent)
        {
            return (parent != null && parent.Kind == Constants.vsProjectItemKindPhysicalFile)
                ? GetFileFromProjectItem(parent)
                : null;
        }

        private IFolderSyntax GetParentFolder(ProjectItem parent)
        {
            return (parent != null && parent.Kind == Constants.vsProjectItemKindPhysicalFolder)
                ? GetFolderFromProjectItem(parent)
                : null;
        }

        private IFolderSyntax GetParentProject(Project project)
        {
            if(project.ParentProjectItem == null)
               return Solution.Project(project.Name);

            var parent = project.ParentProjectItem.Collection.Parent as Project;
            var folder = GetParentProject(parent);
            return ((FolderWrapper)folder).Project(project.Name);
        }
    }
}
