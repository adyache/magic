﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Windows.Forms;

namespace OmniGruntWare.Magic.Fluent
{
    internal class LabelField : FormField
    {
        public LabelField(FormBuilder builder) : base(builder)
        {
        }

        public override IEnumerable<Control> CreateControls()
        {
            yield return new Label
            {
                Text = Caption,
            };
        }

        [ExcludeFromCodeCoverage]
        protected override object ValueFromString(string value)
        {
            throw new NotSupportedException();
        }

        [ExcludeFromCodeCoverage]
        protected override string ValueToString(object value)
        {
            throw new NotSupportedException();
        }

        [ExcludeFromCodeCoverage]
        public override object GetValueFromControl(Control control)
        {
            throw new NotSupportedException();
        }

        [ExcludeFromCodeCoverage]
        public override IEnumerable<string> Validate(object value)
        {
            yield break;
        }
    }
}