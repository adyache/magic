// This file is part of Magic, a Visual Studio extension for development automation
// Copyright � 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using System.Linq;
using EnvDTE;
using EnvDTE80;
using OmniGruntWare.Magic.DteAdapters;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic.Fluent
{
    internal abstract class FileContainer : IFileContainerSyntax
    {
        protected readonly DTE2 _dte2;
        protected readonly FileContainer _parent;
        protected readonly FolderWrapper _folder;
        protected readonly string _name;

        protected FileContainer(DTE2 dte2, FileContainer parent, FolderWrapper folder, string name)
        {
            _dte2 = dte2;
            _parent = parent;
            _folder = folder;
            _name = name;
        }

        public virtual string Path
        {
            get { return System.IO.Path.Combine(_folder.Path, _name); }
        }

        public IFolderSyntax Folder
        {
            get { return _folder; }
        }

        public string Name
        {
            get { return _name; }
        }

        public abstract IFileSyntax File(string name);

        public ICreateFileSyntax CreateFile(string name)
        {
            if (GetFiles().Concat(_folder.GetFiles()).Contains(name))
                throw new ArgumentException(string.Format("File '{0}' already exists", name));

            Create();
            return new FileWrapper(_dte2, this, this as FolderWrapper ?? _folder, name);
        }

        public IEnumerable<string> GetFiles()
        {
            var collection = Collection;
            return collection == null
                ? Enumerable.Empty<string>()
                : collection.OfType<FileDteItem>().Select(x => x.Name).ToList();
        }

        public abstract DteItemCollection Collection { get; }
        protected internal abstract DteItemCollection Create();
    }
}