﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System.Collections.Generic;
using System.Windows.Forms;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic.Fluent
{
    internal class IntegerField : FormField, IPromptIntSyntax, IPromptIntLabelSyntax, IPromptIntMinSyntax, IPromptIntMaxSyntax
    {
        private int _min;
        private int _max;

        public IntegerField(FormBuilder builder) : base(builder)
        {
        }

        public IPromptIntLabelSyntax Label(string label)
        {
            Caption = label;
            return this;
        }

        public IPromptIntMinSyntax Minimum(int value)
        {
            _min = value;
            return this;
        }

        public IPromptIntMaxSyntax Maximum(int value)
        {
            _max = value;
            return this;
        }

        public IRequiredSyntax Default(int value)
        {
            return SetDefault(value, false);
        }

        public IRequiredSyntax PreviousDefaultOr(int value)
        {
            var dv = GetLastValue();
            return SetDefault(value, dv != null && (int)dv >= _min && (int)dv <= _max);
        }

        public override IEnumerable<Control> CreateControls()
        {
            yield return new Label
            {
                Name = ControlName + "_Label",
                Text = Caption,
            };
            yield return new NumericUpDown
            {
                Name = ControlName,
                Minimum = _min,
                Maximum = _max,
                Increment = 1,
                Value = (int)GetDefaultValue(),
            };
        }

        protected override object ValueFromString(string value)
        {
            int result;
            if (!int.TryParse(value, out result))
                return null;
            return result;
        }

        protected override string ValueToString(object value)
        {
            return value.ToString();
        }

        public override object GetValueFromControl(Control control)
        {
            return ValueFromString(control.Text);
        }

        public override IEnumerable<string> Validate(object value)
        {
            if (IsRequired && value == null)
                yield return CleanCaption + " is required";
        }
    }
}