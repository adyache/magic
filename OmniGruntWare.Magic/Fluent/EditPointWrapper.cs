﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EnvDTE;
using OmniGruntWare.Magic.Extensions;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic.Fluent
{
    internal class EditPointWrapper : IBetweenAndSyntax
    {
        private readonly FileWrapper _fileWrapper;
        private readonly EditPoint _startPoint;
        private readonly EditPoint _endPoint;
        private bool _alphabetical;

        private const int FindOptions = (int)(vsFindOptions.vsFindOptionsRegularExpression | vsFindOptions.vsFindOptionsMatchCase | vsFindOptions.vsFindOptionsMatchInHiddenText);

        private const int ReplaceOptions =
            (int)(vsEPReplaceTextOptions.vsEPReplaceTextAutoformat | vsEPReplaceTextOptions.vsEPReplaceTextNormalizeNewlines | vsEPReplaceTextOptions.vsEPReplaceTextTabsSpaces);

        public EditPointWrapper(FileWrapper fileWrapper, TextDocument document, string startRegex, string endRegex)
        {
            _fileWrapper = fileWrapper;
            var searchPoint = document.StartPoint.CreateEditPoint();
            var endPoint = document.StartPoint.CreateEditPoint();
            searchPoint.StartOfDocument();

            if (startRegex != null)
            {
                if (!searchPoint.FindPattern(startRegex, FindOptions, ref endPoint))
                    throw new ArgumentException("Starting location not found");
                endPoint.EndOfLine();
                if (endPoint.AtEndOfDocument)
                    endPoint.Insert(Environment.NewLine);
                else
                {
                    endPoint.LineDown();
                    endPoint.StartOfLine();
                }
                _startPoint = endPoint.CreateEditPoint();
                searchPoint = endPoint.CreateEditPoint();
            }

            if (endRegex == null)
                return;

            switch (endRegex)
            {
                case CloseOfBody:
                    var lineCount = 0;
                    var body = string.Empty;
                    var lines = SplitIntoLines(_startPoint.GetText(document.EndPoint));
                    foreach (var line in lines)
                    {
                        body += line;
                        var openCount = body.Count(c => c == '{');
                        var closeCount = body.Count(c => c == '}');
                        if (closeCount == openCount + 1)
                            break;
                        lineCount++;
                    }
                    endPoint = _startPoint.CreateEditPoint();
                    endPoint.LineDown(lineCount);
                    break;

                default:
                    if (!searchPoint.FindPattern(endRegex, FindOptions, ref endPoint))
                        throw new ArgumentException("End location not found");
                    break;
            }
            endPoint.StartOfLine();
            _endPoint = endPoint.CreateEditPoint();
        }

        public const string CloseOfBody = "_!_}_";

        public IExistingFileSyntax InsertMembers(params string[] bodies)
        {
            if (!_alphabetical)
                return InsertText(bodies);

            var existingCode = SplitIntoMembers(_startPoint.GetText(_endPoint));

            var combinedLines = existingCode.Members
                .Concat(bodies)
                .OrderBy(GetMemberName)
                .Combine(existingCode.BottomComments);

            var newText = CombineLines(combinedLines);
            _startPoint.ReplaceText(_endPoint, newText, ReplaceOptions);
            return _fileWrapper;
        }

        private IExistingFileSyntax InsertText(params string[] lines)
        {
            (_endPoint ?? _startPoint).ReplaceText(0, CombineLines(lines), ReplaceOptions);
            return _fileWrapper;
        }

        private static string GetMemberName(string text)
        {
            var end = text.IndexOfAny(new[] {'{', '('});
            if (end < 0) end = text.Length;
            var lastWord = text.Substring(0, end).Trim().Split().Last();
            return lastWord.TrimStart('_').ToLower();
        }

        public IExistingFileSyntax InsertLines(params string[] lines)
        {
            if (!_alphabetical)
                return InsertText(lines);

            var existingLines = SplitIntoLinesNoEmpties(_startPoint.GetText(_endPoint));
            var combinedLines = existingLines.Concat(lines).OrderBy(GetLineOrder);
            var newText = CombineLines(combinedLines);
            _startPoint.ReplaceText(_endPoint, newText, ReplaceOptions);

            return _fileWrapper;
        }

        private static string GetLineOrder(string line)
        {
            return line.Replace(" ", "").Replace("\t", "").ToLower();
        }

        private static string CombineLines(IEnumerable<string> combinedLines)
        {
            return string.Join(Environment.NewLine, combinedLines) + Environment.NewLine;
        }

        private static IEnumerable<string> SplitIntoLines(string text)
        {
            return text.Replace("\r", "").TrimEnd('\n').Split('\n');
        }

        private static IEnumerable<string> SplitIntoLinesNoEmpties(string text)
        {
            return text.Replace("\r", "").TrimEnd('\n').Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
        }

        private static RangeMembers SplitIntoMembers(string text)
        {
            var results = new RangeMembers();

            var block = new StringBuilder();
            var blockString = string.Empty;
            var lines = SplitIntoLines(text);

            foreach (var line in lines)
            {
                block.AppendLine(line);
                blockString = block.ToString();
                var openCount = blockString.Count(c => c == '{');
                var closeCount = blockString.Count(c => c == '}');
                if (openCount > 0 && openCount == closeCount)
                {
                    results.Members.Add(blockString.TrimEnd());
                    block.Length = 0;
                }
            }
            if (block.Length > 0)
            {
                block.Length -= Environment.NewLine.Length;
                results.BottomComments = block.ToString();
            }

            return results;
        }

        public ILocationSyntax Alphabetically()
        {
            _alphabetical = true;
            return this;
        }
    }
}
