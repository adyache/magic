﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic.Fluent
{
    internal class DateField : FormField, IPromptDateSyntax, IPromptDateLabelSyntax, IPromptDateMinSyntax, IPromptDateMaxSyntax
    {
        private DateTime _min = DateTime.MinValue;
        private DateTime _max = DateTime.MaxValue;

        public DateField(FormBuilder builder)
            : base(builder)
        {
        }

        public IPromptDateLabelSyntax Label(string label)
        {
            Caption = label;
            return this;
        }

        public IRequiredSyntax Default(DateTime value)
        {
            return SetDefault(value, false);
        }

        public IRequiredSyntax PreviousDefaultOr(DateTime value)
        {
            var dv = GetLastValue();
            return SetDefault(value, dv != null && (DateTime)dv >= _min && (DateTime)dv <= _max);
        }

        public override IEnumerable<Control> CreateControls()
        {
            yield return new Label
            {
                Name = ControlName + "_Label",
                Text = Caption,
            };
            yield return new DateTimePicker
            {
                MinDate = _min,
                MaxDate = _max,
                Name = ControlName,
                Format = DateTimePickerFormat.Short,
                Value = (DateTime)GetDefaultValue(),
            };
        }

        protected override object ValueFromString(string value)
        {
            DateTime result;
            DateTime.TryParse(value, out result);
            return result;
        }

        protected override string ValueToString(object value)
        {
            return ((DateTime)value).ToString("d");
        }

        public override object GetValueFromControl(Control control)
        {
            return ((DateTimePicker)control).Value;
        }

        public override IEnumerable<string> Validate(object value)
        {
            if (IsRequired && DateTime.MinValue.Equals(value))
                yield return CleanCaption + " is required";
        }

        public IPromptDateMinSyntax Minimum(DateTime value)
        {
            _min = value;
            return this;
        }

        public IPromptDateMaxSyntax Maximum(DateTime value)
        {
            _max = value;
            return this;
        }
    }
}