﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic.Fluent
{
    internal abstract class FormField
    {
        private readonly FormBuilder _builder;

        protected FormField(FormBuilder builder)
        {
            _builder = builder;
        }

        public string Name { get; set; }
        public string Caption { get; set; }
        public string CleanCaption { get { return Caption.Replace("&", "").TrimEnd(':'); } }
        protected object DefaultValue { get; set; }
        public bool UsePreviousDefaultWhenAvailable { get; set; }
        public Dictionary<string, bool> IsEnabledBy = new Dictionary<string, bool>();
        public bool IsRequired { get; set; }

        public IPromptSelectEnterableSyntax SetDefault(object value, bool usePreviousDefaultWhenAvailable)
        {
            DefaultValue = value;
            UsePreviousDefaultWhenAvailable = usePreviousDefaultWhenAvailable;
            return _builder;
        }

        internal void EnabledBy(string name, bool enabled)
        {
            IsEnabledBy.Add(name, enabled);
        }

        public string ControlName
        {
            get { return "formField_" + new string(Name.Where(char.IsLetterOrDigit).ToArray()); }
        }

        protected object GetDefaultValue()
        {
            return UsePreviousDefaultWhenAvailable
                ? GetLastValue() ?? DefaultValue
                : DefaultValue;
        }

        protected object GetLastValue()
        {
            var setting = MagicSettings.Default.Settings.FirstOrDefault(x => x.Key == Name);
            if (setting.Key == null)
                return null;

            return ValueFromString(setting.Value);
        }

        public void SetLastValue(object value)
        {
            MagicSettings.Default.Settings.RemoveAll(x => x.Key == Name);
            MagicSettings.Default.Settings.Add(new KeyValuePair<string, string>(Name, ValueToString(value)));
        }

        public abstract IEnumerable<Control> CreateControls();
        protected abstract object ValueFromString(string value);
        protected abstract string ValueToString(object value);
        public abstract object GetValueFromControl(Control control);
        public abstract IEnumerable<string> Validate(object value);
    }
}