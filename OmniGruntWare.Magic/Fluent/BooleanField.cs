﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System.Collections.Generic;
using System.Windows.Forms;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic.Fluent
{
    internal class BooleanField : FormField, IPromptBoolSyntax, IPromptBoolLabelSyntax
    {
        public BooleanField(FormBuilder builder) : base(builder)
        {
        }

        public IPromptBoolLabelSyntax Label(string label)
        {
            Caption = label;
            return this;
        }

        public IDisabledBySyntax Default(bool value)
        {
            return SetDefault(value, false);
        }

        public IDisabledBySyntax PreviousDefaultOr(bool value)
        {
            return SetDefault(value, true);
        }

        public override IEnumerable<Control> CreateControls()
        {
            yield return new CheckBox
            {
                Name = ControlName,
                Text = Caption,
                Checked = (bool)GetDefaultValue(),
            };
        }

        protected override object ValueFromString(string value)
        {
            bool result;
            bool.TryParse(value, out result);
            return result;
        }

        protected override string ValueToString(object value)
        {
            return value.ToString();
        }

        public override object GetValueFromControl(Control control)
        {
            return ((CheckBox)control).Checked;
        }

        public override IEnumerable<string> Validate(object value)
        {
            yield break;
        }
    }
}