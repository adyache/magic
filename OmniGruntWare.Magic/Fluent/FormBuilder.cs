﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Windows.Forms;
using EnvDTE80;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic.Fluent
{
    internal class FormBuilder : IPromptSelectEnterableSyntax
    {
        private readonly DTE2 _dte2;
        private readonly string _title;
        private readonly List<FormField> _fields;
        private int _minWidth;
        private Func<IReadOnlyDictionary<string, object>, IEnumerable<string>> _validator;

        public FormBuilder(DTE2 dte2, string title)
        {
            _dte2 = dte2;
            _title = title;
            _fields = new List<FormField>();
        }

        public ICompletedPromptSyntax MinWidth(int width)
        {
            _minWidth = width;
            return this;
        }

        public ICompletedPromptSyntax Instructions(string text)
        {
            _fields.Add(new LabelField(this) {Caption = text});
            return this;
        }

        public IPromptIntSyntax ForInteger(string name)
        {
            return AddAndReturn(new IntegerField(this) {Name = name});
        }

        public IPromptDateSyntax ForDate(string name)
        {
            return AddAndReturn(new DateField(this) {Name = name});
        }

        public IPromptStringSyntax ForString(string name)
        {
            return AddAndReturn(new StringField(this) {Name = name});
        }

        public IPromptSelectSyntax ForSelection(string name)
        {
            return AddAndReturn(new DropdownField(this) {Name = name});
        }

        public IPromptBoolSyntax ForBoolean(string name)
        {
            return AddAndReturn(new BooleanField(this) {Name = name});
        }

        private T AddAndReturn<T>(T field)
            where T : FormField
        {
            _fields.Add(field);
            return field;
        }

        private FormField LastField
        {
            get { return _fields[_fields.Count - 1]; }
        }

        public ICompletedPromptSyntax DisabledBy(string name)
        {
            LastField.EnabledBy(name, false);
            return this;
        }

        public ICompletedPromptSyntax EnabledBy(string name)
        {
            LastField.EnabledBy(name, true);
            return this;
        }

        public IDisabledBySyntax Required()
        {
            LastField.IsRequired = true;
            return this;
        }

        public IRequiredSyntax AllowFreeEntry()
        {
            (LastField as DropdownField).AllowFreeEntry = true;
            return this;
        }

        public IPreparedPromptSyntax Validate(Func<IReadOnlyDictionary<string, object>, IEnumerable<string>> validator = null)
        {
            _validator = validator;
            return this;
        }

        [ExcludeFromCodeCoverage]
        public IReadOnlyDictionary<string, object> Run()
        {
            using (var form = Construct())
            {
                return form.ShowDialog() == DialogResult.OK ? form.Results : null;
            }
        }

        internal frmPrompt Construct()
        {
            return new frmPrompt(_fields, _title, _minWidth, _validator);
        }
    }
}