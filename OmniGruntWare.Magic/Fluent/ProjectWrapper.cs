﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System.Linq;
using EnvDTE;
using EnvDTE80;
using OmniGruntWare.Magic.DteAdapters;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic.Fluent
{
    internal class ProjectWrapper : FolderWrapper, IProjectSyntax
    {
        public ProjectWrapper(DTE2 dte2, FolderWrapper parent, string name)
            : base(dte2, parent, name)
        {
        }

        protected internal override DteItemCollection Create()
        {
            //creating project is not supported, assume project already exists
            return Collection;
        }

        public override DteItemCollection Collection
        {
            get
            {
                try
                {
                    return _parent.Collection[_name].Collection;
                }
                catch
                {
                    return null;
                }
            }
        }

        public override string Path
        {
            get { return (Collection as ProjectDteItemCollection).Path; }
        }
    }
}
