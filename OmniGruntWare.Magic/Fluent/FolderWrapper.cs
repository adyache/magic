﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System.Collections.Generic;
using System.Linq;
using EnvDTE;
using EnvDTE80;
using OmniGruntWare.Magic.DteAdapters;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic.Fluent
{
    internal class FolderWrapper : FileContainer, IFolderSyntax
    {
        public FolderWrapper(DTE2 dte2, FolderWrapper parent, string name)
            : base(dte2, parent, parent, name)
        {
        }

        IFolderSyntax IFolderSyntax.Folder(string name)
        {
            return new FolderWrapper(_dte2, this, name);
        }

        public IEnumerable<string> GetFolders()
        {
            var collection = Collection;
            return collection == null
                ? Enumerable.Empty<string>()
                : collection.Where(x => x is FolderDteItem || x is SolutionFolderDteItem).Select(x => x.Name).ToList();
        }

        public override DteItemCollection Collection
        {
            get
            {
                try
                {
                    return _parent.Collection[_name].Collection;
                }
                catch
                {
                    return null;
                }
            }
        }

        public override IFileSyntax File(string name)
        {
            return new FileWrapper(_dte2, this, this, name);
        }

        protected internal override DteItemCollection Create()
        {
            var collection = _folder.Create();
            var item = collection[_name] ?? collection.AddFolder(_name);

            return item.Collection;
        }

        public IProjectSyntax Project(string name)
        {
            return new ProjectWrapper(_dte2, this, name);
        }
    }
}
