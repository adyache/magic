﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic.Fluent
{
    internal class DropdownField : FormField, IPromptSelectSyntax, IPromptSelectLabelSyntax, IPromptSelectFromSyntax
    {
        private string[] _options;

        public bool AllowFreeEntry { get; set; }

        public DropdownField(FormBuilder builder)
            : base(builder)
        {
        }

        public IPromptSelectLabelSyntax Label(string label)
        {
            Caption = label;
            return this;
        }

        public IPromptSelectFromSyntax From(params string[] options)
        {
            _options = options;
            return this;
        }

        public IPromptSelectEnterableSyntax Default(string value)
        {
            return SetDefault(value, false);
        }

        public IPromptSelectEnterableSyntax PreviousDefaultOr(string value)
        {
            return SetDefault(value, true);
        }

        public override IEnumerable<Control> CreateControls()
        {
            yield return new Label
            {
                Name = ControlName + "_Label",
                Text = Caption,
            };
            var comboBox = new ComboBox
            {
                Name = ControlName,
                DropDownStyle = AllowFreeEntry ? ComboBoxStyle.DropDown : ComboBoxStyle.DropDownList,
                Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
            };
            comboBox.Items.AddRange(_options.Cast<object>().ToArray());
            comboBox.SelectedIndex = Array.IndexOf(_options, (string)GetDefaultValue());
            yield return comboBox;
        }

        protected override object ValueFromString(string value)
        {
            return value;
        }

        protected override string ValueToString(object value)
        {
            return (string)value;
        }

        public override object GetValueFromControl(Control control)
        {
            return control.Text;
        }

        public override IEnumerable<string> Validate(object value)
        {
            if (IsRequired && string.IsNullOrWhiteSpace((string)value))
                yield return CleanCaption + " is required";
        }
    }
}