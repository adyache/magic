﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.Shell;
using VSLangProj;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic
{
    internal static class SpellScanner
    {
        private static IList<ISpell> _spells;
        private static readonly IList<ISpell> _internalSpells;
        private static readonly object _configurationLock = new object();

        public static string ConfigurationError { get; private set; }

        public static DTE2 GetDte2()
        {
            var dte2 = Package.GetGlobalService(typeof (DTE)) as DTE2;
            if (dte2 == null)
                throw new Exception("Cannot get an instance of DTE2");

            return dte2;
        }

        static SpellScanner()
        {
            AppDomain.CurrentDomain.AssemblyResolve += ResolveInternalAssembly;
            _internalSpells = GetAssemblySpells(Assembly.GetExecutingAssembly());
            GetConfiguration();
        }

        private static Assembly ResolveInternalAssembly(object sender, ResolveEventArgs e)
        {
            var wandAssembly = typeof(ISpell).Assembly;
            return e.Name.Split(',')[0] == wandAssembly.GetName().Name ? wandAssembly : null;
        }

        public static IList<ISpell> GetConfiguration()
        {
            var dte2 = GetDte2();
            lock (_configurationLock)
            {
                if (_spells == null)
                {
                    try
                    {
                        var solution = dte2.Solution;
                        if (solution.IsOpen)
                        {
                            var configProject = solution.Projects.Cast<Project>().FirstOrDefault(ContainsConfiguration);
                            if (configProject != null)
                            {
                                ConfigurationError = null;
                                var fileUrLs = (Array) configProject.ConfigurationManager.ActiveConfiguration.OutputGroups.Item("Built").FileURLs;
                                var path = new Uri(fileUrLs.Cast<string>().First()).LocalPath;
                                var bytes = File.ReadAllBytes(path);
                                var assembly = Assembly.Load(bytes);
                                _spells = _internalSpells.Concat(GetAssemblySpells(assembly)).ToList();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ConfigurationError = ex.ToString();
                    }
                }
                return _spells ?? _internalSpells;
            }
        }

        private static IList<ISpell> GetAssemblySpells(Assembly assembly)
        {
            var spellTypes = assembly.GetTypes().Where(x => !x.IsAbstract && typeof (ISpell).IsAssignableFrom(x));
            return spellTypes
                .Select(x => (ISpell)x.GetConstructor(Type.EmptyTypes).Invoke(null))
                .OrderBy(x => x.Name.Replace("&", ""))
                .ToList();
        }

        private static bool ContainsConfiguration(Project project)
        {
            var proj = project.Object as VSProject;
            if (proj == null)
                return false;

            var wandAssemblyName = typeof (ISpell).Assembly.GetName().Name;
            var references = proj.References.Cast<Reference>().Select(x => x.Name);
            return references.Contains(wandAssemblyName);
        }

        public static void InvalidateConfiguration()
        {
            lock (_configurationLock)
            {
                _spells = null;
            }
        }
    }
}
