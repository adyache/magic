﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace OmniGruntWare.Magic
{
    [Serializable]
    public class MagicSettings
    {
        public List<KeyValuePair<string, string>> Settings { get; set; }

        public MagicSettings()
        {
            Settings = new List<KeyValuePair<string, string>>();
        }

        public static MagicSettings Default = Load();

        private const string LineBreakToken = "~cr~";
        private const string FileName = "MagicSettings.ini";

        private static MagicSettings Load()
        {
            var result = new MagicSettings();

            var path = GetPath();
            Debug.WriteLine("Loading settings from " + path);
            if (!File.Exists(path)) return result;

            var lines = File.ReadAllLines(path);
            for (var i = 0; i < lines.Length; i += 2)
            {
                var key = lines[i].Split(new[] { '=' }, 2)[1];
                var value = lines[i + 1].Split(new[] { '=' }, 2)[1].Replace(LineBreakToken, Environment.NewLine);

                result.Settings.Add(new KeyValuePair<string, string>(key, value));
                Debug.WriteLine("{0}={1}", key, value);
            }

            return result;
        }

        private static string GetPath()
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), FileName);
        }

        public void Save()
        {
            var path = GetPath();
            Debug.WriteLine("Saving settings to " + path);

            var lines = new List<string>();
            foreach (var setting in Settings)
            {
                Debug.WriteLine("{0}={1}", setting.Key, setting.Value);
                lines.Add(string.Format("Name={0}", setting.Key));
                lines.Add(string.Format("Value={0}", setting.Value.Replace(Environment.NewLine, LineBreakToken)));
            }

            File.WriteAllLines(path, lines);
        }
    }
}
