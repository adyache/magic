﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.ComponentModel.Design;
using System.Windows.Forms;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using OmniGruntWare.Magic.Fluent;
using OmniGruntWare.Magic.Wand;
using OmniGruntWare.Magic.Spells;
using System.Threading;

namespace OmniGruntWare.Magic
{
    /// <summary>
    /// This is the class that implements the package exposed by this assembly.
    ///
    /// The minimum requirement for a class to be considered a valid package for Visual Studio
    /// is to implement the IVsPackage interface and register itself with the shell.
    /// This package uses the helper classes defined inside the Managed Package Framework (MPF)
    /// to do it: it derives from the Package class that provides the implementation of the 
    /// IVsPackage interface and uses the registration attributes defined in the framework to 
    /// register itself and its components with the shell.
    /// </summary>
    // This attribute tells the PkgDef creation utility (CreatePkgDef.exe) that this class is
    // a package.
    [PackageRegistration(UseManagedResourcesOnly = true)]
    // This attribute is used to register the information needed to show this package
    // in the Help/About dialog of Visual Studio.
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 401)]
    // This attribute is needed to let the shell know that this package exposes some menus.
    [ProvideMenuResource("Menus.ctmenu", 1)]
    [ProvideAutoLoad("{f1536ef8-92ec-443c-9ed7-fdadf150da82}")]
    [Guid(GuidList.guidMagicPkgString)]
    internal sealed class MagicPackage : Package
    {
        /// <summary>
        /// Default constructor of the package.
        /// Inside this method you can place any initialization code that does not require 
        /// any Visual Studio service because at this point the package object is created but 
        /// not sited yet inside Visual Studio environment. The place to do all the other 
        /// initialization is the Initialize method.
        /// </summary>
        public MagicPackage()
        {
            Debug.WriteLine(string.Format(CultureInfo.CurrentCulture, "Entering constructor for: {0}", this.ToString()));
        }


        /////////////////////////////////////////////////////////////////////////////
        // Overridden Package Implementation
        #region Package Members

        private readonly SolutionEventListener _buildEventListener = new SolutionEventListener();
        private readonly List<MenuCommand> _menuCommands = new List<MenuCommand>();

        /// <summary>
        /// Initialization of the package; this method is called right after the package is sited, so this is the place
        /// where you can put all the initialization code that rely on services provided by VisualStudio.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();

            InitializeMenuCommands(true);

            _buildEventListener.RefreshNeeded += RefreshMenuCommands;
            _buildEventListener.Initialize(GetService<IVsSolutionBuildManager>(), GetService<IVsSolution>());
        }

        private T GetService<T>() where T : class
        {
            return GetService(typeof(T)) as T
                ?? Package.GetGlobalService(typeof(T)) as T;
        }

        private void RefreshMenuCommands()
        {
            SpellScanner.InvalidateConfiguration();
            InitializeMenuCommands();
        }

        private void InitializeMenuCommands(bool firstRun = false, int retryCount = 0)
        {
            // Add our command handlers for menu (commands must exist in the .vsct file)
            var mcs = GetService<IMenuCommandService>();
            if (null == mcs)
                return;

            IList<ISpell> spells;

            try
            {
                spells = SpellScanner.GetConfiguration();
            }
            catch
            {
                if (retryCount < 5)
                {
                    RetryLater(firstRun, retryCount + 1);
                }
                return;
            }

            if (firstRun)
            {
                CreateStaticMenuItems(mcs);
                CreateMenuItemsForInternalSpells(mcs, spells);
            }

            RemoveMenuCommandsForSolutionSpells(mcs);
            CreateMenuItemsForSolutionSpells(mcs, spells);

            CreateMenuItemForConfigurationError(mcs, SpellScanner.ConfigurationError);
        }

        private async void RetryLater(bool firstRun, int retryCount)
        {
            await System.Threading.Tasks.Task.Delay(2000);
            InitializeMenuCommands(firstRun, retryCount);
        }

        private void CreateMenuItemForConfigurationError(IMenuCommandService mcs, string configurationError)
        {
            var item = CreateMenuItem(PkgCmdIDList.cmdShowError, (sender, e) => MessageBox.Show(configurationError, "Error Getting Solution Spells"));
            item.Visible = !string.IsNullOrEmpty(configurationError);
            mcs.AddCommand(item);
            _menuCommands.Add(item);
        }

        private void RemoveMenuCommandsForSolutionSpells(IMenuCommandService mcs)
        {
            foreach (var menuCommand in _menuCommands)
                mcs.RemoveCommand(menuCommand);
            _menuCommands.Clear();
        }

        private void CreateMenuItemsForSolutionSpells(IMenuCommandService mcs, IEnumerable<ISpell> spells)
        {
            foreach (var item in CreateMenuItemsForSpells(PkgCmdIDList.cmdSolutionPlaceholder, spells.Where(x => !(x is Spell))))
            {
                mcs.AddCommand(item);
                _menuCommands.Add(item);
            }
        }

        private static void CreateMenuItemsForInternalSpells(IMenuCommandService mcs, IEnumerable<ISpell> spells)
        {
            foreach (var item in CreateMenuItemsForSpells(PkgCmdIDList.cmdInternalPlaceholder, spells.Where(x => x is Spell)))
                mcs.AddCommand(item);
        }

        private static IEnumerable<OleMenuCommand> CreateMenuItemsForSpells(uint startingCommandId, IEnumerable<ISpell> spells)
        {
            var cmdId = (int)startingCommandId;
            foreach (var spell in spells)
                yield return CreateMenuItemForSpell(cmdId++, spell);
        }

        private static void CreateStaticMenuItems(IMenuCommandService mcs)
        {
            mcs.AddCommand(CreateMenuItem(PkgCmdIDList.cmdOptions, NotImplemented));
            mcs.AddCommand(CreateMenuItem(PkgCmdIDList.cmdHelp, ShowAboutBox));
        }

        [ExcludeFromCodeCoverage]
        private static void ShowAboutBox(object sender, EventArgs e)
        {
            using (var about = new AboutBox())
                about.ShowDialog();
        }

        private static MenuCommand CreateMenuItem(uint commandId, EventHandler handler)
        {
            var menuCommandId = new CommandID(GuidList.guidMagicCmdSet, (int)commandId);
            var menuCommand = new MenuCommand(handler, menuCommandId)
            {
                Visible = handler != NotImplemented,
            };
            return menuCommand;
        }

        private static OleMenuCommand CreateMenuItemForSpell(int commandId, ISpell spell)
        {
            var menuCommandId = new CommandID(GuidList.guidMagicCmdSet, commandId);
            return new OleMenuCommand((sender, e) => SafeExecute(spell), menuCommandId, spell.Name)
            {
                Visible = true,
            };
        }

        private static void SafeExecute(ISpell spell)
        {
            try
            {
                var dte2 = SpellScanner.GetDte2();
                spell.Execute(new DevelopmentEnvironmentWrapper(dte2));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error Performing Spell");
            }
        }

        [ExcludeFromCodeCoverage]
        private static void NotImplemented(object sender, EventArgs e)
        {
            MessageBox.Show("This command has not been implemented", "Not implemented");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _buildEventListener.Shutdown(GetService<IVsSolutionBuildManager>(), GetService<IVsSolution>());
            }

            base.Dispose(disposing);
        }

        #endregion
    }
}
