﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System.Collections.Generic;
using System.Linq;

namespace OmniGruntWare.Magic.Extensions
{
    public static class Extensions
    {
        public static IEnumerable<T> Combine<T>(this T singleElement, IEnumerable<T> collection)
        {
            return new[] {singleElement}.Concat(collection);
        }

        public static IEnumerable<T> Combine<T>(this IEnumerable<T> collection, T singleElement)
        {
            return collection.Concat(new[] {singleElement});
        }
    }
}
