﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;

namespace OmniGruntWare.Magic.DteAdapters
{
    internal class ProjectDteItem : DteItem
    {
        private Project _project;

        public ProjectDteItem(Project project)
        {
            _project = project;
        }

        public override string Name
        {
            get { return _project.Name; }
        }

        public override DteItemCollection Collection
        {
            get { return (_project.ProjectItems != null) ? new ProjectDteItemCollection(_project.ProjectItems) : null; }
        }
    }
}
