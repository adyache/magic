﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;

namespace OmniGruntWare.Magic.DteAdapters
{
    internal abstract class ProjectItemsDteItemCollection : DteItemCollection
    {
        protected ProjectItems _projectItems;

        protected ProjectItemsDteItemCollection(ProjectItems projectItems)
        {
            _projectItems = projectItems;
        }

        public override DteItem this[string name]
        {
            get
            {
                try
                {
                    var item = _projectItems.Item(name);
                    //in solution folders, the wrong item can sometimes(?) be returned
                    if (item == null || item.Name != name)
                        return null;

                    return DteItem.FromProjectItem(item);
                }
                catch (ArgumentException)
                {
                    return null;
                }
            }
        }

        public override IEnumerator<DteItem> GetEnumerator()
        {
            return _projectItems.Cast<ProjectItem>().Select(DteItem.FromProjectItem).GetEnumerator();
        }

        public override void AddFromFile(string path)
        {
            new FileDteItem(_projectItems.AddFromFile(path));
        }

        public override DteItem AddFolder(string name)
        {
            if (_projectItems.Kind == Constants.vsProjectItemsKindSolutionItems)
                throw new NotSupportedException("Adding solution folders is not supported");

            return DteItem.FromProjectItem(_projectItems.AddFolder(name));
        }
    }
}
