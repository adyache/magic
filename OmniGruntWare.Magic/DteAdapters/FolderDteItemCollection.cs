﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;

namespace OmniGruntWare.Magic.DteAdapters
{
    internal class FolderDteItemCollection : ProjectItemsDteItemCollection
    {
        public FolderDteItemCollection(ProjectItems projectItems) 
            : base(projectItems)
        {
        }
    }
}
