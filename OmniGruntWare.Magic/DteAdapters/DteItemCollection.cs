﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OmniGruntWare.Magic.DteAdapters
{
    internal abstract class DteItemCollection : IEnumerable<DteItem>
    {
        public abstract DteItem this[string name] { get; }
        public abstract IEnumerator<DteItem> GetEnumerator();
        public abstract void AddFromFile(string path);
        public abstract DteItem AddFolder(string _name);

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
