﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;

namespace OmniGruntWare.Magic.DteAdapters
{
    internal abstract class DteItem
    {
        public abstract string Name { get; }
        public abstract DteItemCollection Collection { get; }
        
        public static DteItem FromProjectItem(ProjectItem item)
        {
            switch (item.Kind)
            {
                case Constants.vsProjectItemKindPhysicalFile:
                    return new FileDteItem(item);

                case Constants.vsProjectItemKindPhysicalFolder:
                    return new FolderDteItem(item);

                case Constants.vsProjectItemKindSolutionItems:
                    var project = item.Object as Project;
                    return project != null
                        ? FromProject(project)
                        : new FileDteItem(item);

                default:
                    return null;
            }
        }

        public static DteItem FromProject(Project project)
        {
            switch (project.Kind)
            {
                case Constants.vsProjectKindSolutionItems:
                case Constants.vsProjectKindMisc:
                    return new SolutionFolderDteItem(project);

                default:
                    return new ProjectDteItem(project);
            }
        }
    }
}
