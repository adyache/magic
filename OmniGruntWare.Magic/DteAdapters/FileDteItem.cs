﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;

namespace OmniGruntWare.Magic.DteAdapters
{
    internal class FileDteItem : DteItem
    {
        private ProjectItem _projectItem;

        public FileDteItem(ProjectItem item)
        {
            _projectItem = item;
        }

        public override string Name
        {
            get { return _projectItem.Name; }
        }

        public override DteItemCollection Collection
        {
            get { return (_projectItem.ProjectItems != null) ? new FileDteItemCollection(_projectItem.ProjectItems) : null; }
        }

        internal ProjectItem ProjectItem { get { return _projectItem; } }
    }
}
