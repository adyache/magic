﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;

namespace OmniGruntWare.Magic.DteAdapters
{
    internal class ProjectDteItemCollection : ProjectItemsDteItemCollection
    {
        public ProjectDteItemCollection(ProjectItems projectItems)
            : base(projectItems)
        {
        }

        public string Path
        {
            get { return System.IO.Path.GetDirectoryName(_projectItems.ContainingProject.FullName); }
        }
    }
}
