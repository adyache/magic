﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;

namespace OmniGruntWare.Magic.DteAdapters
{
    internal class SolutionDteItemCollection : DteItemCollection
    {
        private Projects _projects;

        public SolutionDteItemCollection(Solution solution)
        {
            _projects = solution.Projects;
        }

        public override DteItem this[string name] { get { return DteItem.FromProject(_projects.Cast<Project>().FirstOrDefault(x => x.Name == name)); } }

        public override IEnumerator<DteItem> GetEnumerator()
        {
            return _projects.Cast<Project>().Select(DteItem.FromProject).GetEnumerator();
        }

        public override void AddFromFile(string path)
        {
            throw new NotSupportedException("Creating projects is not supported");
        }

        public override DteItem AddFolder(string name)
        {
            throw new NotSupportedException("Creating solution folders is not supported");
        }
    }
}
