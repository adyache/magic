﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;

namespace OmniGruntWare.Magic.DteAdapters
{
    internal class FileDteItemCollection : ProjectItemsDteItemCollection
    {
        public FileDteItemCollection(ProjectItems projectItems)
            : base(projectItems)
        {
        }
    }
}
