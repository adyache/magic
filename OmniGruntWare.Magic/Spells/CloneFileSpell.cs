﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic.Spells
{
    internal class CloneFileSpell : Spell
    {
        public override string Name
        {
            get { return "&Clone File"; }
        }

        private string _source;
        internal CloneSpellArgs Args;

        public override void Execute(IDevelopmentEnvironment env)
        {
            var activeDocument = env.ActiveFile;
            if (activeDocument == null)
                throw new Exception("No active file");

            var mainDocument = activeDocument.MainFile;
            var folder = mainDocument.Folder;
            var contents = mainDocument.GetFiles().ToDictionary(x => x, x => mainDocument.File(x).Contents);
            _source = mainDocument.Name;
            CloneSpellArgs arguments;

            if (Args == null)
            {
                var args = env.Prompt("Clone " + _source)
                    .Instructions("Clone the file to one or more modified copies" + Environment.NewLine + "by replacing the specified text in file name and contents:")
                    .Instructions("(To create multiple files, enter replacements on separate lines)")
                    .ForString("Replace").Label("&Replace text:").Default(Path.GetFileNameWithoutExtension(_source)).Required()
                    .ForString("With").Label("&With:").Multiline().Default("").Required()
                    .Validate(ParseAndValidate)
                    .Run();

                if (args == null)
                    return;

                arguments = new CloneSpellArgs(args);
            }
            else
            {
                arguments = Args;
                var errors = string.Join(Environment.NewLine, Validate(arguments));
                if(!string.IsNullOrEmpty(errors))
                    throw new Exception(errors);
            }

            foreach (var target in arguments.Targets)
            {
                var mainTargetName = _source.Replace(arguments.Replace, target);
                var mainTarget = folder
                    .CreateFile(mainTargetName)
                    .WithBody(mainDocument.Contents.Replace(arguments.Replace, target));

                foreach (var source in contents.Keys)
                {
                    mainTarget
                        .CreateFile(source.Replace(arguments.Replace, target))
                        .WithBody(contents[source].Replace(arguments.Replace, target));
                }

                mainTarget.Open();
            }
        }

        private IEnumerable<string> ParseAndValidate(IReadOnlyDictionary<string, object> args)
        {
            return Validate(new CloneSpellArgs(args));
        }

        private IEnumerable<string> Validate(CloneSpellArgs parsedArgs)
        {
            if (!_source.Contains(parsedArgs.Replace))
                yield return "Source file name must contain the Replace string";
            if (parsedArgs.Targets.Contains(parsedArgs.Replace))
                yield return "The Replace string cannot be one of the With strings";
            if (parsedArgs.Targets.Distinct().Count() < parsedArgs.Targets.Count())
                yield return "The Replace strings must be unique";
        }
    }

    internal class CloneSpellArgs
    {
        public string Replace;
        public string[] Targets;

        internal CloneSpellArgs()
        {
        }

        public CloneSpellArgs(IReadOnlyDictionary<string, object> args)
        {
            Replace = args["Replace"].ToString();
            Targets = args["With"].ToString().Split(new[] { Environment.NewLine }, StringSplitOptions.None).Select(x => x.Trim()).Where(x => x.Length > 0).ToArray();
        }
    }
}
