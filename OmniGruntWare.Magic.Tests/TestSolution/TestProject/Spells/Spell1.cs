﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OmniGruntWare.Magic.Wand;

namespace TestProject.Spells
{
    internal class Spell1 : ISpell
    {
        /// <summary>
        /// The name of the spell to use as the text of the menu item on the Magic menu.  May include an ampersand to indicate a hotkey.
        /// </summary>
        public string Name
        {
            get
            {
                var name = "&Sample Spell";
                return name;
            }
        }

        public void Execute(IDevelopmentEnvironment env)
        {
            //prompt user for some values
            var args = env.Prompt("Prompt Title")
                .Instructions("Enter the values below:")
                .ForString("Trick").Label("&Trick to perform:").Default("focus").Required()
                .ForInteger("Airspeed").Label("&Airspeed").Minimum(0).Maximum(300).Default(100).Required()
                .Validate(validator);

            //args will be null if the user cancels the prompt
            if (args == null)
                return;

            //perform the function requested
        }

        //perform validations on the set of user entries.
        //if we're here, all entries are guaranteed to exist, and required entries are guaranteed to be non-empty.
        private IEnumerable<string> validator(IReadOnlyDictionary<string, object> args)
        {
            if ((string)args["Trick"] == "fly" && (int)args["Airspeed"] < 65)
                yield return "Cannot take off at this speed";
        }
    }
}
