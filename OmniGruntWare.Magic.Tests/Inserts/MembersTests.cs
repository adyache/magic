// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VSSDK.Tools.VsIdeTesting;
using OmniGruntWare.Magic.Tests.Framework;
using OmniGruntWare.Magic.Wand;
using OmniGruntWare.Magic.Extensions;

namespace OmniGruntWare.Magic.Tests.Inserts
{
    [TestClass]
    public class MembersTests : MagicTest
    {
        private const string FolderName = "Inserts";

        private const string ExistingBody = @"namespace TestProject.Inserts
{
    class <0>
    {
        //Top

        //Non-code comments at the bottom
        //Bottom
    }
}
";

        private static readonly string[] ExistingMembers =
        {
            @"
        /*
        method with a block comment
        */
        public void D()
        {
        }",

            @"
        //single-line auto property with a comment
        private static short B { get; set; }",
        };

        private static readonly string[] MembersToInsert =
        {
            @"
        public void a()
        {
            if //line break here to allow for default spacing variations in dev environments
                (this != null)
            {
                return;
            }
        }",

            @"
        private int _e;
        protected int e
        {
            get { return _e; }
            set
            {
                _e = value;
            }
        }",

            @"
        internal string c(string source)
        {
            return source + source;
        }",
        };

        [TestMethod, HostType("VS IDE")]
        public void InsertMembersBefore()
        {
            OnUiThread(() =>
            {
                var file = Dte.Project(TestSetup.TestProjectName).Folder(FolderName).File("MembersBefore.cs");
                file.Open().Before("//Bottom").InsertMembers(MembersToInsert);

                AssertInserted(file, MembersToInsert, 4, 26);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void InsertMembersAfter()
        {
            OnUiThread(() =>
            {
                var file = Dte.Project(TestSetup.TestProjectName).Folder(FolderName).File("MembersAfter.cs");
                file.Open().After("//Top").InsertMembers(MembersToInsert);

                AssertInserted(file, MembersToInsert, 4, 26);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void InsertMembersBetween()
        {
            OnUiThread(() =>
            {
                var file = Dte.Project(TestSetup.TestProjectName).Folder(FolderName).File("MembersBetween.cs");
                file.Open().Between("//Top").And("//Bottom").InsertMembers(MembersToInsert);

                AssertInserted(file, "        //Existing code at the top".Combine(MembersToInsert), 4, 27);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void InsertMembersBetweenAlphabetic()
        {
            OnUiThread(() =>
            {
                var file = Dte.Project(TestSetup.TestProjectName).Folder(FolderName).File("MembersBetweenAlphabetic.cs");
                file.Open().Between("//Top").And("//Bottom").Alphabetically().InsertMembers(MembersToInsert);

                var orderedMembers = new[] { MembersToInsert[0], MembersToInsert[2], MembersToInsert[1] };
                AssertInserted(file, orderedMembers.Combine("        //Non-member code at the bottom"), 4, 27);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void InsertMembersBetweenAlphabeticAddingToExisting()
        {
            var className = MethodBase.GetCurrentMethod().Name;
            var bodyTemplate = ExistingBody.Replace("<0>", className);
            var templateLines = bodyTemplate.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            var bodyLines = templateLines.Take(5).Concat(ExistingMembers).Concat(templateLines.Skip(5));
            var body = string.Join(Environment.NewLine, bodyLines);

            OnUiThread(() =>
            {
                var file = Dte.Project(TestSetup.TestProjectName).Folder(FolderName)
                    .CreateFile(className + ".cs")
                    .WithBody(body);
                file.Between("//Top").And("//Bottom").Alphabetically().InsertMembers(MembersToInsert);

                var orderedMembers = new[] { MembersToInsert[0], ExistingMembers[1], MembersToInsert[2], ExistingMembers[0], MembersToInsert[1] };
                AssertInserted(file, orderedMembers.Combine(Environment.NewLine + "        //Non-code comments at the bottom"), 4, 38);
            });
        }

        private void AssertInserted(IFileSyntax file, IEnumerable<string> members, int skip, int take)
        {
			CollectionAssertExtensions.AreEqual( AddMarkers( GetLines( members ) ), GetLines( file, skip, take ) );
        }

        private IEnumerable<string> GetLines(IEnumerable<string> members)
        {
            return string.Join(Environment.NewLine, members).Replace("\r", "").Split('\n');
        }
    }
}
