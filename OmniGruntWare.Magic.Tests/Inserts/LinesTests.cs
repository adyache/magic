﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OmniGruntWare.Magic.Tests.Framework;
using OmniGruntWare.Magic.Wand;
using OmniGruntWare.Magic.Extensions;

namespace OmniGruntWare.Magic.Tests.Inserts
{
    [TestClass]
    public class LinesTests : MagicTest
    {
        private const string FolderName = "Inserts";

        protected static readonly string[] LinesToInsert =
        {
            "        int a = 1;",
            "        int c = 3;",
            "        int b = 2;",
        };

        private const string ExistingBody = @"namespace TestProject.Inserts
{
    class <0>
    {
        void Ignore()
        {
            {
            }
        }

        private void SetValues(short s, System.Collections.Generic.IEnumerable<string> names)
        {
            {
                var existing = 5;
            }
        }

        #region Alpha
        byte existingVariable = 6;
        string existingAtBottom = ""7"";
        #endregion
    }
}
";

        [TestMethod, HostType("VS IDE")]
        public void InsertLinesBefore()
        {
            OnUiThread(() =>
            {
                var file = Dte.Project(TestSetup.TestProjectName).Folder(FolderName).File("LinesBefore.cs");
                file.Open().Before("//Bottom").InsertLines(LinesToInsert);

                AssertInserted(file, LinesToInsert, 4, 5);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void InsertLinesAfter()
        {
            OnUiThread(() =>
            {
                var file = Dte.Project(TestSetup.TestProjectName).Folder(FolderName).File("LinesAfter.cs");
                file.Open().After("//Top").InsertLines(LinesToInsert);

                AssertInserted(file, LinesToInsert, 4, 5);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void InsertLinesBetween()
        {
            OnUiThread(() =>
            {
                var file = Dte.Project(TestSetup.TestProjectName).Folder(FolderName).File("LinesBetween.cs");
                file.Open().Between("//Top").And("//Bottom").InsertLines(LinesToInsert);

                AssertInserted(file, "        //Existing code at the top".Combine(LinesToInsert), 4, 6);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void InsertLinesBetweenAlphabetic()
        {
            OnUiThread(() =>
            {
                var file = Dte.Project(TestSetup.TestProjectName).Folder(FolderName).File("LinesBetweenAlphabetic.cs");
                file.Open().Between("//Top").And("//Bottom").Alphabetically().InsertLines(LinesToInsert);

                AssertInserted(file, LinesToInsert.OrderBy(x => x), 4, 5);
            });
        }

        [TestMethod, HostType("VS IDE")]
        [ExpectedExceptionMessage("Starting location not found")]
        public void InsertLinesInvalidStartLocation()
        {
            OnUiThread(() =>
            {
                var file = Dte.Project(TestSetup.TestProjectName).Folder(FolderName).File("LinesBetweenAlphabetic.cs");
                file.Open().After("//Top2").InsertLines(LinesToInsert);
            });
        }

        [TestMethod, HostType("VS IDE")]
        [ExpectedExceptionMessage("End location not found")]
        public void InsertLinesInvalidEndLocation()
        {
            OnUiThread(() =>
            {
                var file = Dte.Project(TestSetup.TestProjectName).Folder(FolderName).File("LinesBetweenAlphabetic.cs");
                file.Open().Before("//Bottom2").InsertLines(LinesToInsert);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void InsertLinesInMethod()
        {
            var className = MethodBase.GetCurrentMethod().Name;
            var body = ExistingBody.Replace("<0>", className);

            OnUiThread(() =>
            {
                var file = Dte.Project(TestSetup.TestProjectName).Folder(FolderName)
                    .CreateFile(className + ".cs")
                    .WithBody(body);
                file.InMethod("SetValues").InsertLines(LinesToInsert);

                AssertInsertedInExistingBody(file, body, LinesToInsert.Select(x => "    " + x), 15);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void InsertLinesInRegion()
        {
            var className = MethodBase.GetCurrentMethod().Name;
            var body = ExistingBody.Replace("<0>", className);

            OnUiThread(() =>
            {
                var file = Dte.Project(TestSetup.TestProjectName).Folder(FolderName)
                    .CreateFile(className + ".cs")
                    .WithBody(body);
                file.InRegion("Alpha").Alphabetically().InsertLines(LinesToInsert);

                AssertInsertedInExistingBody(file, body, LinesToInsert.OrderBy(x => x), 19);
            });
        }

        private static void AssertInsertedInExistingBody(IFileSyntax file, string existingBody, IEnumerable<string> inserted, int atLine)
        {
            var lines = existingBody.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            var expectedLines = lines.Take(atLine).Concat(inserted).Concat(lines.Skip(atLine)).ToList();
            CollectionAssertExtensions.AreEqual(expectedLines, GetLines(file, 0, 1000));
        }

        private static void AssertInserted(IFileSyntax file, IEnumerable<string> lines, int skip, int take)
        {
			CollectionAssertExtensions.AreEqual( AddMarkers( lines ), GetLines( file, skip, take ) );
        }
    }
}
