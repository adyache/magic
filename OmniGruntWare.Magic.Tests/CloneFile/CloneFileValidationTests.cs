// This file is part of Magic, a Visual Studio extension for development automation
// Copyright � 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VSSDK.Tools.VsIdeTesting;
using OmniGruntWare.Magic.Spells;
using OmniGruntWare.Magic.Tests.Framework;

namespace OmniGruntWare.Magic.Tests.CloneFile
{
    [TestClass]
    public class CloneFileValidationTests : CloneFileTestBase
    {
        private const string SourceName = "Class1.cs";

        [TestMethod, HostType("VS IDE")]
        [ExpectedExceptionMessage("No active file")]
        public void CloneFileFailsIfNoActiveFile()
        {
            OnUiThread(() =>
            {
                CloseAllDocuments();
                ExecuteCloneSpell("1", "2", "3");
            });
        }

        [TestMethod, HostType("VS IDE")]
        [ExpectedExceptionMessage("Source file name must contain the Replace string")]
        public void CloneFileFailsWithInvalidReplace()
        {
            OnUiThread(() =>
            {
                Dte.Project(TestSetup.TestProjectName).File(SourceName).Open();
                ExecuteCloneSpell("2", "3");
            });
        }

        [TestMethod, HostType("VS IDE")]
        [ExpectedExceptionMessage("The Replace string cannot be one of the With strings")]
        public void CloneFileFailsWithNopReplace()
        {
            OnUiThread(() =>
            {
                Dte.Project(TestSetup.TestProjectName).File(SourceName).Open();
                ExecuteCloneSpell("1", "2", "1");
            });
        }

        [TestMethod, HostType("VS IDE")]
        [ExpectedExceptionMessage("The Replace strings must be unique")]
        public void CloneFileFailsWithDuplicateReplaces()
        {
            OnUiThread(() =>
            {
                Dte.Project(TestSetup.TestProjectName).File(SourceName).Open();
                ExecuteCloneSpell("1", "4", "4");
            });
        }

        [TestMethod, HostType("VS IDE")]
        [ExpectedExceptionMessage("File 'Form1.cs' already exists")]
        public void CloneFileFailsWhenExistingFileFound()
        {
            OnUiThread(() =>
            {
                Dte.Project(TestSetup.TestProjectName).File(SourceName).Open();
                ExecuteCloneSpell("Class", "Form");
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void CloneSpellArgsConstructorTest()
        {
            var args = new CloneSpellArgs(new Dictionary<string, object>
            {
                {"Replace", "Rpl"},
                {"With", string.Join(Environment.NewLine, new[]
                    {
                        "\t",
                        "One",
                        " ",
                        "Two",
                        "Three",
                        "",
                    })},
            });
            
            Assert.AreEqual("Rpl", args.Replace);
            CollectionAssert.AreEquivalent(new[] {"One", "Two", "Three"}, args.Targets);
        }
    }
}