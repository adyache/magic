﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VSSDK.Tools.VsIdeTesting;

namespace OmniGruntWare.Magic.Tests.CloneFile
{
    [TestClass]
    public class CloneFileTestInFolder : CloneFileTestBase
    {
        private const string FolderName = "Folder1";
        private const string SourceName = "ClassInFolder1.cs";
        private string _sourceContents;

        [TestMethod, HostType("VS IDE")]
        public void CloneFileWorksInFolder()
        {
            OnUiThread(() =>
            {
                var file = Dte.Project(TestSetup.TestProjectName).Folder(FolderName).File(SourceName).Open();
                _sourceContents = file.Contents;

                ExecuteCloneSpell("1", "4", "3");

                AssertCloned("1", "4");
                AssertCloned("1", "3");
            });
        }

        private void AssertCloned(string source, string replace)
        {
            var targetName = SourceName.Replace(source, replace);
            var target = Dte.Project(TestSetup.TestProjectName).Folder(FolderName).File(targetName);
            
            Assert.IsTrue(target.Exists);
            Assert.AreEqual(_sourceContents.Replace(source, replace), target.Contents);
        }
    }
}
