// This file is part of Magic, a Visual Studio extension for development automation
// Copyright � 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VSSDK.Tools.VsIdeTesting;

namespace OmniGruntWare.Magic.Tests.CloneFile
{
    [TestClass]
    public class CloneFileTestWithWindowsForm : CloneFileTestBase
    {
        private const string SourceName = "Form1.cs";
        private string _mainSourcContents;
        private Dictionary<string, string> _sourceContents;

        [TestMethod, HostType("VS IDE")]
        public void CloneFileWorksWithWindowsForm()
        {
            OnUiThread(() =>
            {
                var file = Dte.Project(TestSetup.TestProjectName).File(SourceName).Open();
                _mainSourcContents = file.Contents;
                _sourceContents = file.GetFiles().ToDictionary(x => x, x => file.File(x).Contents);

                ExecuteCloneSpell("Form1", "Form2", "Form3");

                AssertCloned("Form1", "Form2");
                AssertCloned("Form1", "Form3");
            });
        }

        private void AssertCloned(string source, string replace)
        {
            var mainTargetName = SourceName.Replace(source, replace);
            var mainTarget = Dte.Project(TestSetup.TestProjectName).File(mainTargetName);

            Assert.IsTrue(mainTarget.Exists);
            Assert.AreEqual(_mainSourcContents.Replace(source, replace), mainTarget.Contents);
            Assert.AreEqual(2, mainTarget.GetFiles().Count());
            Assert.AreEqual(2, _sourceContents.Count);

            foreach (var sourceName in _sourceContents.Keys)
            {
                var targetName = sourceName.Replace(source, replace);
                var target = mainTarget.File(targetName);

                Assert.IsTrue(target.Exists);
                Assert.AreEqual(_sourceContents[sourceName].Replace(source, replace), target.Contents);
            }
        }
    }
}
