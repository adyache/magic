﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using EnvDTE80;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VSSDK.Tools.VsIdeTesting;
using OmniGruntWare.Magic.Extensions;
using OmniGruntWare.Magic.Fluent;
using OmniGruntWare.Magic.Wand;
using Microsoft.VsSDK.UnitTestLibrary;

namespace OmniGruntWare.Magic.Tests
{
    public abstract class MagicTest
    {
        internal DevelopmentEnvironmentWrapper Dte;

        protected MagicTest()
        {
            Dte = new DevelopmentEnvironmentWrapper((DTE2)VsIdeTestHostContext.Dte);
        }

        protected static void OnUiThread(Action action)
        {
            Exception exception = null;

            UIThreadInvoker.Invoke(new Action(() =>
            {
                try
                {
                    action();
                }
                catch (Exception ex)
                {
                    exception = new Exception(ex.Message, ex);
                }
            }));

            //provides call stack info from within the invoked action, which is otherwise lost because of invoking
            if(exception != null)
                throw exception;
        }

        protected void CloseAllDocuments()
        {
            if(Dte.ActiveFile != null)
                ExecuteDteCommand("Window.CloseAllDocuments");
        }

        protected static List<string> GetLines(IFileSyntax file, int skip, int take)
        {
            return file.Contents.Split(new[] {Environment.NewLine}, StringSplitOptions.None).Skip(skip).Take(take).ToList();
        }

        protected static List<string> AddMarkers(IEnumerable<string> lines)
        {
            return "        //Top".Combine(lines).Combine("        //Bottom").ToList();
        }

        protected static T GetService<T>() where T : class
        {
            var service = GetServiceFromTestProvider<T>() ?? GetServiceFromPackage<T>();

            Assert.IsNotNull(service);
            return service;
        }

        private static T GetServiceFromTestProvider<T>() where T : class
        {
            return VsIdeTestHostContext.ServiceProvider.GetService(typeof (T)) as T;
        }

        private static IVsPackage _magicPackage;

        private static T GetServiceFromPackage<T>() where T : class
        {
            if (_magicPackage == null)
            {
                _magicPackage = new MagicPackage() as IVsPackage;
                var serviceProvider = OleServiceProvider.CreateOleServiceProviderWithBasicServices();
                Assert.AreEqual(VSConstants.S_OK, _magicPackage.SetSite(serviceProvider), "SetSite failed");
            }

            var method = typeof(Package).GetMethod("GetService", BindingFlags.Instance | BindingFlags.NonPublic);
            var service = method.Invoke(_magicPackage, new[] { typeof(T) }) as T;
            return service;
        }

        protected static void RebuildTestSolution()
        {
            var mgr = GetService<IVsSolutionBuildManager>();
            var sln = GetService<IVsSolution>();

            var listener = new SolutionEventListener();
            listener.Initialize(mgr, sln);
            var refreshed = false;
            listener.RefreshNeeded += () => refreshed = true;

            ExecuteDteCommand("File.SaveAll");
            ExecuteDteCommand("Build.BuildSolution");

            while (!refreshed)
            {
                Debug.WriteLine("Waiting for a solution rebuild");
                System.Threading.Thread.Sleep(1000);
            }

            listener.Shutdown(mgr, sln);
        }

        private static void ExecuteDteCommand(string commandName)
        {
            for (var i = 10; i >= 0; i--)
            {
                try
                {
                    VsIdeTestHostContext.Dte.ExecuteCommand(commandName);
                    return;
                }
                catch
                {
                    if(i == 0) throw;
                    System.Threading.Thread.Sleep(1000);
                }
            }
        }
    }
}
