﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System.IO;

namespace OmniGruntWare.Magic.Tests
{
    internal static class DirectoryExtensions
    {
        internal static void CopyDirectory(string sourcePath, string destPath)
        {
            if (Directory.Exists(destPath))
                Directory.Delete(destPath, true);

            Directory.CreateDirectory(destPath);

            foreach (var file in Directory.GetFiles(sourcePath))
            {
                var dest = Path.Combine(destPath, Path.GetFileName(file));
                File.Copy(file, dest);
            }

            foreach (var folder in Directory.GetDirectories(sourcePath))
            {
                var dest = Path.Combine(destPath, Path.GetFileName(folder));
                CopyDirectory(folder, dest);
            }
        }
    }
}
