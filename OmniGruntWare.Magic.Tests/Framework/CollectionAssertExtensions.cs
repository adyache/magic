﻿using System.Collections;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OmniGruntWare.Magic.Tests.Framework
{
	internal class CollectionAssertExtensions
	{
		public static void AreEqual( ICollection expected, ICollection actual )
		{
			Assert.AreEqual( expected.Count, actual.Count, "Collection counts are different" );

			var items = expected.Cast<object>().Zip( actual.Cast<object>(), ( e, a ) => new { e, a } ).ToArray();

			for ( var i = 0; i < items.Length; i++ )
			{
				var exp = items[i].e;
				var act = items[i].a;

				if ( exp is string && act is string )
					AssertStringsAreEqual( exp.ToString(), act.ToString() );
				else
					Assert.AreEqual( exp, act, "Elements at index {0} don't match: expected {1}, actual {2}", i, exp, act );
			}
		}

		private static void AssertStringsAreEqual( string expected, string actual )
		{
			if ( expected == actual )
				return;

			expected = CleanTabs( expected );
			actual = CleanTabs( actual );

			if ( expected == actual )
				return;

			Assert.AreEqual( expected, actual );
		}

		private static string CleanTabs( string source )
		{
			return Regex.Replace( source, "\\s+", "" );
		}
	}
}
