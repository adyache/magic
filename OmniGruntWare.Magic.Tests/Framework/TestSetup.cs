﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using EnvDTE;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VSSDK.Tools.VsIdeTesting;
using OmniGruntWare.Magic.Spells;

namespace OmniGruntWare.Magic.Tests
{
    [TestClass]
    public class TestSetup : MagicTest
    {
        internal const string TestSolutionFolder = "TestSolution";
        internal const string TestSolutionName = "TestSolution.sln";
        internal const string TestProjectName = "TestProject";
        internal const string TestProjectInFolderName = "ProjectInFolder";
        internal const string TestSolutionFolderName = "SolutionFolder";
        internal const string TestTemplateFolder = "SpellsTemplate";

#if DEBUG
        internal const string BuildConfiguration = "Debug";
#else
        internal const string BuildConfiguration = "Release";
#endif

        private static TestContext _testContext;

        internal static Solution TestSolution { get; private set; }
        internal static Project TestProject { get; private set; }
        internal static string UnzippedSpellsTemplatePath { get; private set; }

        [AssemblyInitialize, HostType("VS IDE")]
        public static void OpenTestSolution(TestContext testContext)
        {
            _testContext = testContext;

            OnUiThread(() =>
            {
                var solutionFolder = MakeFreshCopyOfTestSolution();
                UnzippedSpellsTemplatePath = UnzipSpellsTemplate();
                OpenInitialStateTestSolution(solutionFolder);
                TestProject = GetProjectFromTestSolution(TestProjectName);
            });
        }

        [AssemblyCleanup, HostType("VS IDE")]
        public static void SaveTestSolution()
        {
            //delay the save to avoid a race condition
            //System.Threading.Thread.Sleep(1000);

            OnUiThread(() =>
            {
                VsIdeTestHostContext.Dte.ExecuteCommand("File.SaveAll");
            });
        }

        private static string MakeFreshCopyOfTestSolution()
        {
            var testSolutionRelativePath = Path.Combine(typeof(TestSetup).Namespace, TestSolutionFolder);
            var testSolutionSource = GetFullPathInSourceSolution(testSolutionRelativePath);
            var testSolutionTarget = Path.Combine(_testContext.TestRunResultsDirectory, TestSolutionFolder);

            DirectoryExtensions.CopyDirectory(testSolutionSource, testSolutionTarget);

            return testSolutionTarget;
        }

        private static string GetFullPathInSourceSolution(string relativePath)
        {
            //for code coverage using OpenCover, tests must be run in a different directory from the normal VS location.
            //this makes it necessary to go up a few levels (different in these two scenarios) to get to the TestSolution.
            var directory = _testContext.TestDir;
            string sourcePath;
            do
            {
                directory = Path.GetDirectoryName(directory);
                sourcePath = Path.Combine(directory, relativePath);
            } while (!Directory.Exists(sourcePath));
            return sourcePath;
        }

        private static string UnzipSpellsTemplate()
        {
            var templateName = typeof(Spell).Namespace;
            var templateRelativePath = Path.Combine(templateName, "bin", BuildConfiguration, @"ProjectTemplates\CSharp\Extensibility\1033");
            var templateSource = Path.Combine(GetFullPathInSourceSolution(templateRelativePath), templateName + ".zip");
            var templateTarget = Path.Combine(_testContext.TestRunResultsDirectory, TestTemplateFolder);
            
            ZipFile.ExtractToDirectory(templateSource, templateTarget);

            return Path.Combine(templateTarget, templateName + ".vstemplate");
        }

        private static void OpenInitialStateTestSolution(string solutionFolder)
        {
            TestSolution = VsIdeTestHostContext.Dte.Solution;
            var path = Path.Combine(solutionFolder, TestSolutionName);
            TestSolution.Open(path);
            Assert.IsNotNull(TestSolution, "Test solution not found");
        }

        private static Project GetProjectFromTestSolution(string projectName)
        {
            var project = TestSolution.Projects.Cast<Project>().FirstOrDefault(x => x.Name == projectName);
            Assert.IsNotNull(project, "{0} project not found", projectName);
            return project;
        }
    }
}
