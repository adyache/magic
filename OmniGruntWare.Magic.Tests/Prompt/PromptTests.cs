﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VSSDK.Tools.VsIdeTesting;
using OmniGruntWare.Magic.Fluent;
using OmniGruntWare.Magic.Tests.Framework;

namespace OmniGruntWare.Magic.Tests.Prompt
{
    [TestClass]
    public class PromptTests : MagicTest
    {
        private frmPrompt _form;
        private readonly DateTime _minDate = new DateTime(2014, 1, 1);
        private readonly DateTime _maxDate = new DateTime(2014, 2, 15);
        private readonly DateTime _defaultDate = new DateTime(2014, 2, 1);

        private enum TableRow
        {
            Instructions,
            Flag,
            IntLabel,
            Int,
            Enabler,
            Disabler,
            DateLabel,
            Date,
            SingleLineLabel,
            SingleLine,
            MultiLineLabel,
            MultiLine,
            SelectLabel,
            Select,
            FreeSelectLabel,
            FreeSelect,
        }

        [TestInitialize, HostType("VS IDE")]
        public void CreateForm()
        {
            OnUiThread(() =>
            {
                var formBuilder = (FormBuilder)Dte.Prompt("Prompt Caption")
                    .Instructions("Instructions Text")
                    .ForBoolean("Flag").Label("&Flag").Default(true).EnabledBy("Enabler")
                    .ForInteger("Int").Label("&Int 32").Minimum(3).Maximum(5).Default(4).Required().DisabledBy("Disabler")
                    .ForBoolean("Enabler").Label("&Enabler").Default(false)
                    .ForBoolean("Disabler").Label("&Disabler").Default(true)
                    .ForDate("Date").Label("&Date").Minimum(_minDate).Maximum(_maxDate).Default(_defaultDate).Required().DisabledBy("Disabler")
                    .ForString("SingleLine").Label("&Single Line").Default("Single").Required().EnabledBy("Enabler")
                    .ForString("MultiLine").Label("&MultiLine").Multiline().Default("Multi" + Environment.NewLine + "Line")
                    .ForSelection("Select").Label("&Select").From("One", "Two", "Three").Default("Two").EnabledBy("Enabler")
                    .ForSelection("FreeSelect").Label("&Free Select").From("Four", "Five", "Six").Default("Five").AllowFreeEntry().Required()
                    .Validate(Validator);

                _form = formBuilder.Construct();
                _form.Show();
            });
        }

        [TestCleanup, HostType("VS IDE")]
        public void DestroyForm()
        {
            OnUiThread(() =>
            {
                _form.Dispose();
            });
        }

        private static IEnumerable<string> Validator(IReadOnlyDictionary<string, object> args)
        {
            if (string.Equals(args["Select"], args["FreeSelect"]))
                yield return "Two selects cannot be equal";
        }

        [TestMethod, HostType("VS IDE")]
        public void MinWidthTest()
        {
            OnUiThread(() =>
            {
                Assert.IsTrue(_form.Width < 400);

                var builder = (FormBuilder)Dte.Prompt("Prompt Caption").MinWidth(400);

                using (var form = builder.Construct())
                {
                    form.Show();
                    Assert.IsTrue(form.Width == 400);
                }
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void InstructionsControlsTest()
        {
            OnUiThread(() =>
            {
                var c = GetControlInRow<Label>(TableRow.Instructions);
                Assert.AreEqual("Instructions Text", c.Text);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void CheckboxControlsTest()
        {
            OnUiThread(() =>
            {
                var c = GetControlInRow<CheckBox>(TableRow.Flag);
                Assert.AreEqual("formField_Flag", c.Name);
                Assert.AreEqual("&Flag", c.Text);
                Assert.IsTrue(c.Checked);
                Assert.AreEqual(true, new BooleanField(null).GetValueFromControl(c));

                Assert.IsFalse(GetControlInRow<CheckBox>(TableRow.Enabler).Checked);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void IntegerControlsTest()
        {
            OnUiThread(() =>
            {
                var label = GetControlInRow<Label>(TableRow.IntLabel);
                Assert.AreEqual("&Int 32", label.Text);

                var c = GetControlInRow<NumericUpDown>(TableRow.Int);
                Assert.AreEqual("formField_Int", c.Name);
                Assert.AreEqual(3, c.Minimum);
                Assert.AreEqual(4, c.Value);
                Assert.AreEqual(4, new IntegerField(null).GetValueFromControl(c));
                Assert.AreEqual(5, c.Maximum);
                Assert.AreEqual(1, c.Increment);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void DateControlsTest()
        {
            OnUiThread(() =>
            {
                var label = GetControlInRow<Label>(TableRow.DateLabel);
                Assert.AreEqual("&Date", label.Text);

                var c = GetControlInRow<DateTimePicker>(TableRow.Date);
                Assert.AreEqual("formField_Date", c.Name);
                Assert.AreEqual(_minDate, c.MinDate);
                Assert.AreEqual(_defaultDate, c.Value);
                Assert.AreEqual(_defaultDate, new DateField(null).GetValueFromControl(c));
                Assert.AreEqual(_maxDate, c.MaxDate);
                Assert.AreEqual(DateTimePickerFormat.Short, c.Format);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void SingleLineTextboxControlsTest()
        {
            OnUiThread(() =>
            {
                var label = GetControlInRow<Label>(TableRow.SingleLineLabel);
                Assert.AreEqual("&Single Line", label.Text);

                var c = GetControlInRow<TextBox>(TableRow.SingleLine);
                Assert.AreEqual("formField_SingleLine", c.Name);
                Assert.AreEqual("Single", c.Text);
                Assert.AreEqual(c.Text, new StringField(null).GetValueFromControl(c));
                Assert.AreEqual(new TextBox().Height, c.Height);
                Assert.IsFalse(c.Multiline);
                Assert.AreEqual(ScrollBars.None, c.ScrollBars);
                Assert.AreEqual(AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right, c.Anchor);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void MultiLineTextboxControlsTest()
        {
            OnUiThread(() =>
            {
                var label = GetControlInRow<Label>(TableRow.MultiLineLabel);
                Assert.AreEqual("&MultiLine", label.Text);

                var c = GetControlInRow<TextBox>(TableRow.MultiLine);
                Assert.AreEqual("formField_MultiLine", c.Name);
                Assert.AreEqual("Multi" + Environment.NewLine + "Line", c.Text);
                Assert.AreEqual(c.Text, new StringField(null).GetValueFromControl(c));
                Assert.IsTrue(c.Multiline);
                Assert.AreEqual(new TextBox().Height * 6, c.Height);
                Assert.AreEqual(ScrollBars.Both, c.ScrollBars);
                Assert.AreEqual(AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right, c.Anchor);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void DropdownControlsTest()
        {
            OnUiThread(() =>
            {
                var label = GetControlInRow<Label>(TableRow.SelectLabel);
                Assert.AreEqual("&Select", label.Text);

                var c = GetControlInRow<ComboBox>(TableRow.Select);
                Assert.AreEqual("formField_Select", c.Name);
                Assert.AreEqual(ComboBoxStyle.DropDownList, c.DropDownStyle);
                Assert.AreEqual(AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right, c.Anchor);
				CollectionAssertExtensions.AreEqual( new[] { "One", "Two", "Three" }, c.Items );
                Assert.AreEqual("Two", new DropdownField(null).GetValueFromControl(c));
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void FreeDropdownControlsTest()
        {
            OnUiThread(() =>
            {
                var label = GetControlInRow<Label>(TableRow.FreeSelectLabel);
                Assert.AreEqual("&Free Select", label.Text);

                var c = GetControlInRow<ComboBox>(TableRow.FreeSelect);
                Assert.AreEqual("formField_FreeSelect", c.Name);
                Assert.AreEqual(ComboBoxStyle.DropDown, c.DropDownStyle);
                Assert.AreEqual(AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right, c.Anchor);
				CollectionAssertExtensions.AreEqual( new[] { "Four", "Five", "Six" }, c.Items );
                Assert.AreEqual("Five", c.SelectedItem);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void EnabledByTests()
        {
            OnUiThread(() =>
            {
                var dependentControls = new[]
                    {
                        TableRow.Flag,
                        TableRow.SingleLineLabel,
                        TableRow.SingleLine,
                        TableRow.SelectLabel,
                        TableRow.Select,
                    }
                    .Select(GetControlInRow<Control>)
                    .ToList();

                foreach(var c in dependentControls)
                    Assert.IsFalse(c.Enabled, "{0} should be disabled", c.Name);

                GetControlInRow<CheckBox>(TableRow.Enabler).Checked = true;

                foreach(var c in dependentControls)
                    Assert.IsTrue(c.Enabled, "{0} should be enabled", c.Name);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void DisabledByTests()
        {
            OnUiThread(() =>
            {
                var dependentControls = new[]
                    {
                        TableRow.IntLabel,
                        TableRow.Int,
                        TableRow.DateLabel,
                        TableRow.Date,
                    }
                    .Select(GetControlInRow<Control>)
                    .ToList();

                foreach(var c in dependentControls)
                    Assert.IsFalse(c.Enabled, "{0} should be disabled", c.Name);

                GetControlInRow<CheckBox>(TableRow.Disabler).Checked = false;

                foreach(var c in dependentControls)
                    Assert.IsTrue(c.Enabled, "{0} should be enabled", c.Name);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void ValidationTestWithAllValuesValid()
        {
            OnUiThread(() =>
            {
                EnableAllControls();

                Assert.IsFalse(_form.GetValidationErrors().Any());
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void ValidationTestWithCustomValidationErrors()
        {
            OnUiThread(() =>
            {
                EnableAllControls();
                GetControlInRow<ComboBox>(TableRow.FreeSelect).Text = "Two";

                Assert.AreEqual("Two selects cannot be equal", _form.GetValidationErrors().Single());
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void ValidationTestWithSomeControlsDisabled()
        {
            OnUiThread(() =>
            {
                SetUpInvalidValues();

                Assert.AreEqual("Free Select is required", _form.GetValidationErrors().Single());
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void ValidationTestWithAllControlsEnabled()
        {
            OnUiThread(() =>
            {
                SetUpInvalidValues();
                EnableAllControls();

                CollectionAssert.AreEquivalent(new[]
                {
                    "Int 32 is required",
                    "Single Line is required",
                    "Free Select is required",
                }, _form.GetValidationErrors().ToList());
            });
        }

        private void SetUpInvalidValues()
        {
            GetControlInRow<NumericUpDown>(TableRow.Int).Text = null;
            GetControlInRow<TextBox>(TableRow.SingleLine).Text = " ";
            GetControlInRow<TextBox>(TableRow.MultiLine).Text = null;
            GetControlInRow<ComboBox>(TableRow.Select).SelectedItem = null;
            GetControlInRow<ComboBox>(TableRow.FreeSelect).Text = null;
        }

        [TestMethod, HostType("VS IDE")]
        public void PreviousDefaultTest()
        {
            OnUiThread(() =>
            {
                EnableAllControls();
                ClickOkButton();

                var formBuilder = (FormBuilder) Dte.Prompt("Prompt Caption")
                    .Instructions("Empty")
                    .ForBoolean("Flag").Label("&Flag").PreviousDefaultOr(false)
                    .ForInteger("Int").Label("&Int").Minimum(3).Maximum(5).PreviousDefaultOr(5)
                    .ForBoolean("Enabler").Label("&Enabler").PreviousDefaultOr(false)
                    .ForBoolean("Disabler").Label("&Disabler").PreviousDefaultOr(true)
                    .ForDate("Date").Label("&Date").Minimum(_minDate).Maximum(_maxDate).PreviousDefaultOr(_maxDate)
                    .ForString("SingleLine").Label("&SingleLine").PreviousDefaultOr("Not Single")
                    .ForString("MultiLine").Label("&MultiLine").PreviousDefaultOr("Not Multi")
                    .ForSelection("Select").Label("&Select").From("One", "Two", "Three").PreviousDefaultOr("One")
                    .ForSelection("Select2").Label("&Select2").From("One", "Two", "Three").PreviousDefaultOr("One");

                using (var form = formBuilder.Construct())
                {
                    form.Show();

                    AssertControlValue(form, TableRow.Flag, (CheckBox x) => x.Checked, true);
                    AssertControlValue(form, TableRow.Int, (NumericUpDown x) => x.Value, 4);
                    AssertControlValue(form, TableRow.Enabler, (CheckBox x) => x.Checked, true);
                    AssertControlValue(form, TableRow.Disabler, (CheckBox x) => x.Checked, false);
                    AssertControlValue(form, TableRow.Date, (DateTimePicker x) => x.Value, _defaultDate);
                    AssertControlValue(form, TableRow.SingleLine, (TextBox x) => x.Text, "Single");
                    AssertControlValue(form, TableRow.Select, (ComboBox x) => x.Text, "Two");
                    AssertControlValue(form, TableRow.FreeSelect, (ComboBox x) => x.Text, "One");
                }
            });
        }

        private void EnableAllControls()
        {
            GetControlInRow<CheckBox>(TableRow.Enabler).Checked = true;
            GetControlInRow<CheckBox>(TableRow.Disabler).Checked = false;
        }

        private static void AssertControlValue<TControl, TValue>(frmPrompt form, TableRow row, Func<TControl, TValue> getValue, TValue expected) where TControl : Control
        {
            var control = GetControlInRow<TControl>(form, row);
            var actual = getValue(control);
            Assert.AreEqual(expected, actual);
        }

        private T GetControlInRow<T>(TableRow row) where T: Control
        {
            return GetControlInRow<T>(_form, row);
        }

        private static T GetControlInRow<T>(frmPrompt form, TableRow row) where T: Control
        {
            var table = (TableLayoutPanel)form.Controls["table"];
            var control = table.GetControlFromPosition(0, (int)row) as T;
            Assert.IsNotNull(control);
            return control;
        }

        private void ClickOkButton()
        {
            var button = (Button)_form.Controls["btnOK"];
            button.PerformClick();
        }
    }
}
