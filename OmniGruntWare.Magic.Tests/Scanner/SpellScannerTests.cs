﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using EnvDTE80;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VSSDK.Tools.VsIdeTesting;
using OmniGruntWare.Magic.Spells;
using OmniGruntWare.Magic.Wand;
using VSLangProj;

namespace OmniGruntWare.Magic.Tests.Scanner
{
    [TestClass]
    public class SpellScannerTests : MagicTest
    {
        private const string FolderName = "Spells";
        private const string SourceName = "Spell1.cs";

        private const string WorkingSpellResultFileName = "WorkingSpellResult.cs";
        private const string WorkingSpellResultsBody = "//Working Spell Results";
        private const string WorkingSpellBody = @"
using OmniGruntWare.Magic.Wand;

namespace TestProject.Spells
{
    internal class WorkingSpell : ISpell
    {
        public string Name { get { return ""Working Spell""; } }

        public void Execute(IDevelopmentEnvironment env)
        {
            env.Project(""" + TestSetup.TestProjectName + @""").Folder(""" + FolderName + @""")
                .CreateFile(""" + WorkingSpellResultFileName + @""")
                .WithBody(""" + WorkingSpellResultsBody + @""");
        }
    }
}";

        [TestMethod, HostType("VS IDE")]
        public void SpellsAreDetectedWhenSolutionIsLoadedAndUpdatedAfterBuild()
        {
			//NOTE 1. For this test to pass, after first cloning the Magic repository you must build the TestSolution (it is in a folder under the test project).
			//        A fresh copy of the test solution is made for each test run, and the assumption is the solution has already been built.
			//        because the bin folder is excluded from source control, after cloning the repo there will not be a bin folder.
			//        You may also need to rebuild the TestSolution if you make changes to it.

			//NOTE 2. For this test to pass, if you have the Magic extension installed in your dev environment, you must disable it.
			//        Otherwise the installed instance of Magic will interfere with the experimental instance under test.

            //cannot debug tests normally.  Run tests with this command commented out, and select the current IDE.
            //System.Diagnostics.Debugger.Launch();

            RetryAssertSeveralTimes("initial load", AssertSampleSpellIsDetectedOnInitialLoad);

            OnUiThread(ModifySpellName);
            RebuildTestSolution();
            RetryAssertSeveralTimes("updated spell name", AssertUpdatedSpellIsDetected);

            OnUiThread(CreateWorkingSpell);
            RebuildTestSolution();
            RetryAssertSeveralTimes("executing working spell", ExecuteWorkingSpell);
            RetryAssertSeveralTimes("checking working spell results", AssertWorkingSpellResults);

            OnUiThread(CreateBrokenSpell);
            RebuildTestSolution();
            RetryAssertSeveralTimes("broken spell", AssertBrokenSpellResultsInError);

            //this part doesn't work - can't figure out how to make AddFromTempalte work.
            //OnUiThread(CreateSpellsProjectFromTemplate);
            //OnUiThread(RemoveWandReferenceFromOldProject);
            //RebuildTestSolution();
            //RetryAssertSeveralTimes("creating a new spells project", AssertNewSpellDetected);
        }

        private static void AssertSampleSpellIsDetectedOnInitialLoad()
        {
            Assert.IsNull(SpellScanner.ConfigurationError, "Configuration error: " + SpellScanner.ConfigurationError);
            Assert.IsFalse(GetCommand(PkgCmdIDList.cmdShowError).Visible);

            AssertCommands(PkgCmdIDList.cmdInternalPlaceholder, new CloneFileSpell().Name);
            AssertCommands(PkgCmdIDList.cmdSolutionPlaceholder, "&Sample Spell");
        }

        private void ModifySpellName()
        {
            Dte.Project(TestSetup.TestProjectName).Folder(FolderName).File(SourceName)
                .Before("return name;")
                .InsertLines("name = \"&Sample Spell 2\";");
        }

        private static void AssertUpdatedSpellIsDetected()
        {
            Assert.IsNull(SpellScanner.ConfigurationError);
            Assert.IsFalse(GetCommand(PkgCmdIDList.cmdShowError).Visible);

            AssertCommands(PkgCmdIDList.cmdSolutionPlaceholder, "&Sample Spell 2");
        }

        private void CreateWorkingSpell()
        {
            Dte.Project(TestSetup.TestProjectName).Folder(FolderName)
                .CreateFile("WorkingSpell.cs").WithBody(WorkingSpellBody);
        }

        private static void ExecuteWorkingSpell()
        {
            var cmd = GetCommand("Working Spell", PkgCmdIDList.cmdSolutionPlaceholder);
            Assert.IsNotNull(cmd);
            cmd.Invoke();
        }

        private void AssertWorkingSpellResults()
        {
            string actualContents = null;
            try
            {
                actualContents = Dte.Project(TestSetup.TestProjectName).Folder(FolderName).File(WorkingSpellResultFileName).Contents;
            }
            catch
            {
            }
            Assert.AreEqual(WorkingSpellResultsBody, actualContents);
        }

        private void CreateBrokenSpell()
        {
            Dte.Project(TestSetup.TestProjectName).Folder(FolderName).File(SourceName)
                .Before("return name;")
                .InsertLines("throw new Exception(\"Error Test\");");
        }

        private static void AssertBrokenSpellResultsInError()
        {
            Assert.IsTrue(GetCommand(PkgCmdIDList.cmdShowError).Visible);
            Assert.IsTrue(SpellScanner.ConfigurationError.Contains("Error Test"));
        }

        private void CreateSpellsProjectFromTemplate()
        {
            var sln = (Solution2)TestSetup.TestSolution;

            //this doesn't help, in fact I get error 80004005
            ////Get the Shell Service
            //var shellService = GetService<IVsShell>();
            //IVsPackage package;
            //var packageGuid = new Guid(GuidList.guidMagicPkgString);
            //Assert.AreEqual(VSConstants.S_OK, shellService.IsPackageLoaded(ref packageGuid, out package));
            //Assert.IsNotNull(package, "Package failed to load");

            var solutionPath = Path.GetDirectoryName(sln.FullName);
            var projectPath = Path.Combine(solutionPath, "S");
            Directory.CreateDirectory(projectPath);

            sln.AddFromTemplate(TestSetup.UnzippedSpellsTemplatePath, projectPath, "SpellsProject.csproj");
        }

        private static void RemoveWandReferenceFromOldProject()
        {
            var wandAssemblyName = typeof (ISpell).Assembly.GetName().Name;
            var oldProject = (VSProject) TestSetup.TestProject.Object;
            oldProject.References.Cast<Reference>().First(x => x.Name == wandAssemblyName).Remove();
        }

        private void AssertNewSpellDetected()
        {
            AssertCommands(PkgCmdIDList.cmdSolutionPlaceholder, "&Perform Magic");
        }

        private static void RetryAssertSeveralTimes(string context, Action action)
        {
            for (var i = 6; i >= 0; i--)
            {
                try
                {
                    OnUiThread(action);
                    return;
                }
                catch (Exception ex)
                {
                    if (!IsAssertException(ex) || i == 0)
                        throw new Exception("Assert failed on " + context, ex);

                    Debug.WriteLine(ex.Message);
                    Debug.WriteLine("Retrying...");
                    Thread.Sleep(1000);
                }
            }
        }

        private static bool IsAssertException(Exception exception)
        {
            for (var ex = exception; ex != null; ex = ex.InnerException)
                if (ex is AssertFailedException)
                    return true;

            return false;
        }

        private static void AssertCommands(uint startingId, params string[] names)
        {
            var commands = new List<string>();
            while(true)
            {
                var cmd = GetCommand(startingId++);
                if(cmd == null)
                    break;

                commands.Add(GetCommandText(cmd));
            }

            CollectionAssert.AreEquivalent(names, commands);
        }

        private static string GetCommandText(MenuCommand cmd)
        {
            var oleCmd = cmd as OleMenuCommand;
            return oleCmd == null ? null : oleCmd.Text;
        }

        private static MenuCommand GetCommand(string text, uint startingCommandId)
        {
            for(var commandId = startingCommandId; ; commandId++)
            {
                var command = GetCommand(commandId);
                if(command == null)
                    return null;
                if(GetCommandText(command) == text)
                    return command;
            }
        }

        private static MenuCommand GetCommand(uint commandId)
        {
            var mcs = GetService<IMenuCommandService>();
            Assert.IsNotNull(mcs);

            return mcs.FindCommand(new CommandID(GuidList.guidMagicCmdSet, (int)commandId));
        }
    }
}
