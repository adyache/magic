﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VSSDK.Tools.VsIdeTesting;
using OmniGruntWare.Magic.Tests.Framework;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic.Tests.DevEnvironment
{
    [TestClass]
    public class DependentFileTest : MagicTest
    {
        private const string SourceName = "Form1.cs";
        private const string DependentName = "Form1.Designer.cs";
        private const string SecondDependentName = "Form1.resx";

        [TestMethod, HostType("VS IDE")]
        public void ActiveFileReturnsValidWrapper()
        {
            OnUiThread(() =>
            {
                var file = OpenDependentFile();

                Assert.AreEqual(DependentName, file.Name);
                CollectionAssert.AreEquivalent(file.MainFile.GetFiles().ToArray(), new[] {DependentName, SecondDependentName});
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void ActiveFileReferencesValidFolder()
        {
            OnUiThread(() =>
            {
                var file = OpenDependentFile();

                Assert.IsNotNull(file.Folder);
                Assert.AreEqual(file.MainFile.Folder, file.Folder);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void ActiveFileHasCorrectPath()
        {
            OnUiThread(() =>
            {
                var file = OpenDependentFile();

                Assert.IsNotNull(file);
                Assert.AreEqual(file.MainFile.Folder, file.Folder);
            });
        }

        [TestMethod, HostType("VS IDE")]
        [ExpectedExceptionMessage("The requested project item does not exist")]
        public void CannotOpenNonexistingFile()
        {
            OnUiThread(() =>
            {
                var file = Dte.Project(TestSetup.TestProjectName).Folder("Fake").File("Invalid").File("Bad");

                Assert.IsNotNull(file);
                file.Open();
            });
        }

        private IExistingFileSyntax OpenDependentFile()
        {
            Dte.Project(TestSetup.TestProjectName).File(SourceName).File(DependentName).Open();
            return Dte.ActiveFile;
        }
    }
}
