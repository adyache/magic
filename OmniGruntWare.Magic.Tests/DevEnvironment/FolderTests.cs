// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic.Tests.DevEnvironment
{
    [TestClass]
    public class FolderTests : MagicTest
    {
        private const string FolderName = "Folder2";

        [TestMethod, HostType("VS IDE")]
        public void FolderIsCreatedWhenNecessary()
        {
            OnUiThread(() =>
            {
                Func<IFolderSyntax> getFolder = () => 
                    Dte.Project(TestSetup.TestProjectName).Folder(FolderName).Folder("Subfolder");

                var file = getFolder().Folder("Subfolder2").CreateFile("File1.cs").WithBody("//");

                Assert.IsTrue(file.Exists);
                Assert.AreEqual("Subfolder2", getFolder().GetFolders().Single());
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void FileIsCreatedWhenNecessary()
        {
            OnUiThread(() =>
            {
                Func<IFolderSyntax> getFolder = () =>
                    Dte.Project(TestSetup.TestProjectName).Folder(FolderName).Folder("Subfolder3");

                var file = getFolder().File("File2.cs").CreateFile("File3.cs").WithBody("//");

                Assert.IsTrue(file.Exists);
                Assert.AreEqual("", file.MainFile.Contents);
            });
        }
    }
}
