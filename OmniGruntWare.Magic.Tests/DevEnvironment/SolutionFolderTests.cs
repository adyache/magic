﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OmniGruntWare.Magic.Fluent;
using OmniGruntWare.Magic.Tests.Framework;
using OmniGruntWare.Magic.Wand;

namespace OmniGruntWare.Magic.Tests.DevEnvironment
{
    [TestClass]
    public class SolutionFolderTests : MagicTest
    {
        private const string FolderName = "ProjectInFolder";
        private const string ProjectName = "ProjectInFolder";
        private const string SourceName = "ClassX.cs";

        private IExistingFileSyntax OpenFile()
        {
            Dte.Solution.Folder(TestSetup.TestSolutionFolderName).Project(TestSetup.TestProjectInFolderName).File(SourceName).Open();
            return Dte.ActiveFile;
        }

        [TestMethod, HostType("VS IDE")]
        public void ActiveFileInProjectInFolderReturnsValidWrapper()
        {
            OnUiThread(() =>
            {
                var file = OpenFile();

                Assert.AreEqual(SourceName, file.Name);
                CollectionAssert.AreEquivalent(file.Folder.GetFiles().ToArray(), new[] { SourceName });
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void ActiveFileInProjectInFolderReferencesValidFolder()
        {
            OnUiThread(() =>
            {
                var file = OpenFile();

                var project = file.Folder as ProjectWrapper;
                var folder = project.Folder as FolderWrapper;
                Assert.AreEqual(TestSetup.TestProjectInFolderName, project.Name);
                Assert.AreEqual(TestSetup.TestSolutionFolderName, folder.Name);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void FileInSolutionFolderIsCreatedWhenNecessary()
        {
            OnUiThread(() =>
            {
                Func<IFolderSyntax> getFolder = () =>
                    Dte.Solution.Folder(TestSetup.TestSolutionFolderName);

                var file = getFolder().CreateFile("ClassX44.cs").WithBody("//a");

                Assert.IsTrue(file.Exists);
                Assert.AreEqual("//a", file.Contents);
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void FileBelowSolutionFolderIsCreatedWhenNecessary()
        {
            OnUiThread(() =>
            {
                Func<IFolderSyntax> getFolder = () =>
                    Dte.Solution.Folder(TestSetup.TestSolutionFolderName).Project(TestSetup.TestProjectInFolderName);

                var file = getFolder().CreateFile("ClassX22.cs").WithBody("//");

                Assert.IsTrue(file.Exists);
                Assert.AreEqual("//", file.Contents);
            });
        }

        [TestMethod, HostType("VS IDE")]
        [ExpectedExceptionMessage("Adding solution folders is not supported")]
        public void FolderBelowSolutionFolderIsNotCreatedWhenNecessary()
        {
            OnUiThread(() =>
            {
                const string subfolderName = "Another";
                const string fileName = "ClassX33.cs";

                Dte.Solution.Folder(TestSetup.TestSolutionFolderName).Folder(subfolderName).CreateFile(fileName).WithBody("///");
            });
        }
    }
}
