﻿// This file is part of Magic, a Visual Studio extension for development automation
// Copyright © 2014 OmniGruntWare
//
// Magic is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Magic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with Magic.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
//
// Magic documentation can be found at <http://bitbucket.org/adyache/magic/wiki/Home>.
// You can contact the author at alexd60@hotmail.com.

using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OmniGruntWare.Magic.Tests.DevEnvironment
{
    [TestClass]
    public class ActiveFileTests : MagicTest
    {
        private const string FolderName = "Folder2";
        private const string SourceName = "FolderClass1.cs";

        [TestMethod, HostType("VS IDE")]
        public void ActiveFileReturnsValidWrapper()
        {
            OnUiThread(() =>
            {
                Dte.Project(TestSetup.TestProjectName).Folder(FolderName).File(SourceName).Open();

                var file = Dte.ActiveFile;

                Assert.AreEqual(SourceName, file.Name);
                CollectionAssert.AreEquivalent(file.Folder.GetFiles().ToArray(), new[] {SourceName, SourceName.Replace('1', '2')});
            });
        }

        [TestMethod, HostType("VS IDE")]
        public void ActiveFileReturnsNullWhenNone()
        {
            OnUiThread(() =>
            {
                CloseAllDocuments();

                var file = Dte.ActiveFile;

                Assert.IsNull(file);
            });
        }
    }
}
