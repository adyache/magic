REM Bring dev tools into the PATH.
call "C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\Tools\VsDevCmd.bat"

REM Create output directory if necessary
mkdir .\CodeCoverage

REM Run unit tests through OpenCover
.\packages\OpenCover.4.5.2316\OpenCover.Console.exe^
 -register:user^
 -target:MSTest.exe^
 -targetargs:"/noresults /noisolation /testcontainer:OmniGruntWare.Magic.Tests.dll"^
 -targetdir:.\OmniGruntWare.Magic.Tests\bin\Debug^
 -mergebyhash^
 -skipautoprops^
 -filter:+[OmniGruntWare.Magic]*^
 -excludebyattribute:"*.GeneratedCodeAttribute;*.ExcludeFromCodeCoverageAttribute"^
 -excludebyfile:"*.Designer.cs"^
 -hideskipped^
 -output:.\CodeCoverage\output.xml
 
REM Generate the report
.\packages\ReportGenerator.1.9.1.0\ReportGenerator.exe^
 -reports:.\CodeCoverage\output.xml^
 -targetdir:.\CodeCoverage^
 -reporttypes:Html,HtmlSummary

REM Open the report
start .\CodeCoverage\index.htm

pause
exit
